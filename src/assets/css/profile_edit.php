<div class="top_header_space">
	<section class="cartOuter" >
		<div class="container" >
			<div class="accDetailTOp" >
				<h3>Account</h3>
				<p>somebody@example.com</p>
			</div>
			<div class="row" >
				<div class="col-lg-3 col-md-4" >
					<div class="priceDetailBox lefttorightBox jQueryEqualHeight" >
						<h3>ORDERS</h3>
						<ul class="orderlinks" >
							<li><a href="#">Order History</a></li>
						</ul>
						<h3>CREDITS </h3>
						<ul class="orderlinks" >
							<li><a href="#">Vopers Wallet</a></li>
						</ul>
						<h3>ACCOUNT</h3>
						<ul class="orderlinks" >
							<li><a href="#" class="active-links">Profile</a></li>
							<li><a href="#">Addresses</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-9 col-md-8" >
					<div class="shadowWhiteBox jQueryEqualHeight" >
						<div class="cartInner" >
							<h2>Primary Information </h2>

							<div class="order_box">
								<div class="row">
									<div class="col-md-12 col-lg-3"><div class="user_profile"> <img src="images/user.jpg" class="img-fluid" alt="" title="">
										<div class="change_user_id">  <input id="changePicpro" type="file"/>
											<label for="changePicpro" >Change Picture</label> </div>
										</div></div>
										<div class="col-md-12 col-lg-9">
											<?php if(!empty($user_data)){ extract($user_data[0]); ?>
												<div class="row">
													<div class="col-md-4"><div class="profile_tag_name"> Name</div></div>
													<div class="col-md-8"><div class="profile_tag_name"><?php echo $first_name." ".$last_name; ?></div> </div>
												</div>

												<div class="row">
													<div class="col-md-4"><div class="profile_tag_name"> E-mail id	 </div></div>
													<div class="col-md-8"><div class="profile_tag_name"><?php echo $email; ?></div> </div>
												</div>

												<div class="row">
													<div class="col-md-4"><div class="profile_tag_name"> Mobile Number</div></div>
													<div class="col-md-8"><div class="profile_tag_name"><?php echo $phone_number; ?></div> </div>
												</div>

												<div class="row">
													<div class="col-md-4"><div class="profile_tag_name"> Address  </div></div>
													<div class="col-md-8"><div class="profile_tag_name"> <?php echo $address; ?></div> </div>
												</div>
												<div class="row">
													<div class="col-md-12">
														<div class="btn-pink-edit">
															<a href="<?php base_url();?>profile/edit"> Edit Profile </a>
														</div>
													</div>
												</div>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>