import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-edit-address',
  templateUrl: './edit-address.component.html',
  styleUrls: ['./edit-address.component.css']
})
export class EditAddressComponent implements OnInit {
  addressForm: FormGroup;
  message: object;
  profileDataEdit: object;
  submitted = false;
  profileData: any;
  firstName: any;
  base64textString: any;
  editFromimg: any;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  constructor(
    private formBuilder: FormBuilder,
    private DataService: DataService,
    private route: ActivatedRoute,
    private Router: Router,
    private spinner: NgxSpinnerService
  ) {
    this.route.params.subscribe(params => this.getaddressDetailsByID(params['id']));
  }
  getaddressDetailsByID(id: any) {

    this.DataService.getAddressById(id).subscribe(resultAddressDetails => {
      console.log(resultAddressDetails);
      var editFormArr = {};
      this.spinner.hide();
      if (resultAddressDetails.statusCode == 200) {
        localStorage.setItem('deliveryAddressId', resultAddressDetails.responsePacket.deliveryAddressId);
        editFormArr = {
          deliveryAddressId: resultAddressDetails.responsePacket.deliveryAddressId,
          name: resultAddressDetails.responsePacket.fullName,
          mobile_number: resultAddressDetails.responsePacket.mobileNumber,
          building: resultAddressDetails.responsePacket.addressLine1,
          locality: resultAddressDetails.responsePacket.addressLine2,
          pin: resultAddressDetails.responsePacket.pincode,
          city: resultAddressDetails.responsePacket.city,
          state: resultAddressDetails.responsePacket.state,
          fromName: resultAddressDetails.responsePacket.fromName,
          fromContactNumber: resultAddressDetails.responsePacket.fromContactNumber
          // fromAddress: resultAddressDetails.responsePacket.fromAddress,

        };
        this.editFromimg = resultAddressDetails.responsePacket.profileImage;

        this.addressForm.setValue(editFormArr);
        this.profileData = resultAddressDetails.responsePacket;
        this.profileDataEdit = resultAddressDetails.responsePacket
      }
    }

    )
  }
  ngOnInit() {
    this.spinner.show();
    this.addressForm = this.formBuilder.group({
      deliveryAddressId: [''],
      name: ['', Validators.required],
      mobile_number: ['', [Validators.required, Validators.min(1000000000), Validators.max(9999999999)]],
      building: ['', Validators.required],
      locality: ['', Validators.required],
      pin: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      fromName: [''],
      fromContactNumber: ['', [Validators.min(1000000000), Validators.max(9999999999)]]
      // fromAddress: ['']
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.addressForm.controls;
  }
  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.addressForm.invalid) {
      return;
    }
    this.spinner.show();
    //call login api here
    const userId = localStorage.getItem('userId');
    this.DataService.adduserAddress(this.addressForm.value, userId).subscribe(result => {
      this.spinner.hide();
      if (result.statusCode == 200) {
         this.message = result;
        this.Router.navigateByUrl('/view-address');
      } else {
        this.message = result;
      }
    },
      error => {

      })
  }

}
