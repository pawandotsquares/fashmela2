import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
	cartData: any;
	htmlOptions: any = {};
	promoCode: any = {};
	oldPrice: any = {};
	shipment_token: any = {};
	couponDiscountValue: any;
	couponID: any;
	specialDiscountValue: any;
	couponData: any;
  message: object;
	emptyMessage: string;
  userId: any;
	purchaseType: any;
	totalShippingCharge: any;
  quantity: any;
  maxQuantity: any;
  finaPriceValue: number;
  finaMrpValue: number;
  cartDataCount: number;
  discount: number;
	discountPercent: any;
	total : any = 0;
	colorSetImages=[];
	subProArray:object;
	colorSetImg=[];
	sizeList=[];
	subProducts=[];
	Images:any;
	//productId:any;

  public spinnerConfig: any = {
	bdColor: 'rgba(51,51,51,0.8)',
	size: 'large',
	color: '#fff',
	type: 'ball-circus',
	loadigText: 'Loading...'
  };
  constructor(
	private DataService: DataService,
	private spinner: NgxSpinnerService,
	private router : Router
  ) { }

  ngOnInit() {
		this.finaPriceValue =0;
		this.finaMrpValue =0;
		this.htmlOptions = {
			haveApplyCode: true,
			applyCode: false,
			appliedCode: false
		}
	//this.total=0;
	this.spinner.show();
	this.purchaseType = localStorage.getItem('purchaseType');
	this.userId = localStorage.getItem('userId');
	this.cartList(this.userId, this.purchaseType);
	localStorage.setItem('couponDiscountValue', '0');
	localStorage.setItem('shipingCharges', '0');
	localStorage.setItem('specialDiscountValue', '0');
	//this.cartCount(this.userId);
		this.totalShippingCharge = 0;
		//this.getShipingChargesUsingWeight();

	}
	getShipingChargesUsingWeight(){
		this.DataService.shiprocketAuth().subscribe(resultshiprocketAuth => {
			var addressData = {
				pickup_postcode: 302019,
				delivery_postcode: 302004,
				weight: 1,
				cod: 0
			}
			this.shipment_token = resultshiprocketAuth.token;
			//console.log(this.shipment_token);
			if (this.shipment_token) {

				this.DataService.getShipingChargesUsingWeight(this.shipment_token, addressData).subscribe(resultshiprocketTracking => {
					if (resultshiprocketTracking.status == 200) {
						this.totalShippingCharge = resultshiprocketTracking['data']['available_courier_companies'][0]['rate'];
						localStorage.setItem('shipingCharges', this.totalShippingCharge);
					}else{

					}

				},
					error => {

					})

			}
		},
			error => {

			})
	}
	applyPromoCode() {

		if (!this.promoCode.promoCodeText) {
			alert('Please enter valid promo code');
			return;
		}
		if (this.specialDiscountValue>0) {
			alert('Your cart products already have discounted');
			return;
		}
		let promoCode = {
			user: this.userId,
			couponCode: this.promoCode.promoCodeText
		}
		this.oldPrice = this.finaPriceValue;
		var elementButton = <HTMLInputElement>document.getElementById("applyButtonDisabled");
		elementButton.classList.add("disabled");
		this.DataService.applyCoupon(promoCode).subscribe(resultCouponData => {
			elementButton.classList.remove("disabled");

			if (resultCouponData.statusCode == 200) {

				this.couponData = resultCouponData['responsePacket'];
				console.log(this.couponData);
		 if (this.couponData.couponType == 1) {
					let discountPerAmout = (this.couponData.couponValue / 100) * this.finaPriceValue
					this.finaPriceValue = this.finaPriceValue - discountPerAmout;
					this.couponDiscountValue = discountPerAmout;
				} else {
					if (this.couponData.couponValue <= this.finaPriceValue) {
						this.finaPriceValue = this.finaPriceValue - this.couponData.couponValue;
						this.couponDiscountValue = this.couponData.couponValue;
					} else {
						this.couponDiscountValue = this.finaPriceValue;
						this.finaPriceValue = 0;
					}
				}
				this.htmlOptions = {
					haveApplyCode: false,
					applyCode: false,
					appliedCode: true
				}
				localStorage.setItem('couponID', this.couponID);
				localStorage.setItem('couponDiscountValue', this.couponDiscountValue);
				this.htmlOptions.appliedCodeText = this.couponData.couponCode;
			} else {
				this.promoCode.promoCodeText = '';
				alert(resultCouponData['message']);
			}
		},
			error => {

			})
	}
	haveApplyCode() {
		this.htmlOptions = {
			haveApplyCode: false,
			applyCode: true,
			appliedCode: false
		}
	}
	cancelPromoCode() {
		this.promoCode.promoCodeText = "";
		this.htmlOptions = {
			haveApplyCode: true,
			applyCode: false,
			appliedCode: false
		}
	}
	removePromoCode() {
		this.finaPriceValue = this.oldPrice;
		this.couponDiscountValue = 0;
		localStorage.setItem('couponDiscountValue', this.couponDiscountValue);
		this.cancelPromoCode()
		this.htmlOptions = {
			haveApplyCode: true,
			applyCode: false,
			appliedCode: false
		}
	}
  cartList(userId, purchaseType) {
	this.DataService.getCart(userId, purchaseType).subscribe(resultCartData => {
		console.log(resultCartData)
		this.spinner.hide();
	  if (resultCartData.statusCode == 200) {
			this.cartData = resultCartData.responsePacket.cartItems;
			this.specialDiscountValue = 0;
			if (resultCartData['responsePacket']['totalCartDiscount']){
				this.specialDiscountValue = resultCartData['responsePacket']['totalCartDiscount'];
			}
			/* if (this.cartData){
				for (let index = 0; index < this.cartData.length; index++) {
					this.specialDiscountValue = this.specialDiscountValue + this.cartData[index]['itemDiscount'];
				}
			} */
			localStorage.setItem('specialDiscountValue', this.specialDiscountValue);
			for (let cData of this.cartData) {
				if(cData.parentProduct){
					for (let colorImages of cData.parentProduct.subProducts) {
								this.colorSetImages.push(colorImages.images)
							}
				}
			}
			this.colorSetImg = this.colorSetImages;
			this.finaPriceValue = 0;
		  	resultCartData.responsePacket.cartItems.map(obj => {
			this.finaPriceValue = this.finaPriceValue + obj.cartItemTotalPrice;

		//	console.log(this.finaPriceValue);
		 // this.finaPriceValue = this.finaPriceValue + obj.cartItemTotalPrice;
			//this.finaMrpValue =this.finaPriceValue;
		 // this.finaMrpValue = obj.product.mrp;
		  //this.finaMrpValue = this.finaMrpValue + obj.product.mrp;
		});
		  this.finaMrpValue = this.finaPriceValue;
		  if (this.specialDiscountValue > 0 && this.finaPriceValue >= this.specialDiscountValue) {

				this.finaPriceValue = this.finaPriceValue - this.specialDiscountValue;
			}
		/*if (this.finaMrpValue > this.finaPriceValue ){
		  this.discount = this.finaMrpValue - this.finaPriceValue;
		  this.discountPercent = (this.discount / this.finaMrpValue)*100
		}else{
		  this.discount = 0;
		} */
		} else if (resultCartData.statusCode == 202) {
			localStorage.clear()
			alert("Your account has been deactivated, Please login again to proceed");
			this.router.navigateByUrl("/login");
	  } else {
		this.message = resultCartData.message;
		this.emptyMessage = "Your Shopping Cart is empty";
	  }
	}
	)
  }
  // cartCount(userId) {

	// this.spinner.show();
	// this.DataService.getCart(userId,this.purchaseType).subscribe(resultCartCount => {
	//   this.spinner.hide();
	//   if (resultCartCount.statusCode == 200) {
	// 		this.cartDataCount = resultCartCount.responsePacket.totalCartItemCount;
	// 		//this.finaMrpValue = resultCartCount.responsePacket.totalCartAmount;
	// 		this.finaPriceValue =0;
	// 		this.finaMrpValue = 0;
	// 		//console.log(this.finaMrpValue);
	// 		//this.totalShippingCharge = resultCartCount.responsePacket.totalShippingCharge;
	// 		this.totalShippingCharge =0;
	// 		this.finaPriceValue = resultCartCount.responsePacket.totalCartAmount;
	// 		//console.log(this.finaPriceValue);
	// 		//alert(this.finaPriceValue);
	// 		if (this.totalShippingCharge) {
	// 			this.finaPriceValue = this.finaPriceValue + this.totalShippingCharge
	// 		}

	// 	if (this.finaMrpValue > this.finaPriceValue) {
	// 		this.discount = this.finaMrpValue - this.finaPriceValue;
	// 	} else {
	// 		this.discount = 0;
	// 	}
	// 	this.discountPercent = resultCartCount.responsePacket.totalDiscountPercentage;

	//   } else {
	// 	this.message = resultCartCount.message;
	//   }
	// })
  // }
  deleteCartProduct(productCartId) {
	this.spinner.show();

	  this.DataService.removeCartItem(productCartId).subscribe(resultCartData => {
		this.spinner.hide();
	  if (resultCartData.statusCode == 200) {
			this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
			this.router.navigate(["cart"]));
			this.ngOnInit();
	  }else{
		  this.message = resultCartData.message;
			this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
			this.router.navigate(["cart"]));
	  }
	 })
  }

	qtyDecr(cart) {

		if (cart.quantity > 1) {
		this.spinner.show();
		let updateToCartObj = {};

		if(cart.parentProduct){

			this.sizeList = [
				{
					sizeId: cart.parentProduct.subProducts[0].sizeList[0].sizeId
				}
			]
			var userId =  {}
			userId = {
					userId: this.userId
				}

			for (let childProdId of cart.parentProduct.subProducts) {

			 	this.subProducts.push({
						sizeList: this.sizeList,
						productId: childProdId.productId,
						purchaseType:childProdId.purchaseType,
					})
			 }

			 this.subProArray={
					subProducts: this.subProducts,
					productId: cart.parentProduct.productId
				 }

			updateToCartObj = {
				cartId: cart.cartId,
				productCartId: cart.productCartId,
				user: userId,
				quantity: cart.quantity - 1,
				parentProduct: this.subProArray

			}
		}else{
			updateToCartObj = {
				cartId: cart.cartId,
				productCartId: cart.productCartId,
				quantity: cart.quantity - 1,
				product: cart.product
			}
		}


			updateToCartObj = JSON.stringify(updateToCartObj)
			this.DataService.updateCartCount(updateToCartObj).subscribe(result => {
				this.spinner.hide();
				if (result.statusCode == 200) {

					this.ngOnInit();
				}else{
					this.message = result.message;
				}
			})
		}
  }

	qtyIncr(cart) {
		console.log(cart);
		if(cart.parentProduct){
			var cartProductQty = cart.parentProduct.quantity;
		}else{
			cartProductQty = cart.product.quantity;
		}
		//this.finaPriceValue = cart.cartItemTotalPrice
		//this.finaPriceValue = cart.product.totalPrice;
		  //this.finaPriceValue = this.finaPriceValue + obj.product.totalPrice;
			this.finaMrpValue =0;
		//alert(cart.product.quantity);
		//console.log(cartProductQty);
		if (cart.quantity < cartProductQty) {
		this.spinner.show();
		let updateToCartObj = {};
		if(cart.parentProduct){

			this.sizeList = [
				{
					sizeId: cart.parentProduct.subProducts[0].sizeList[0].sizeId
				}
			]
			var userId = {}
			 userId = {
					userId: this.userId
				}

			for (let childProdId of cart.parentProduct.subProducts) {

			 	this.subProducts.push({
						sizeList: this.sizeList,
						productId: childProdId.productId,
						purchaseType:childProdId.purchaseType,
					})
			 }

			 this.subProArray={
					subProducts: this.subProducts,
					productId: cart.parentProduct.productId
				 }

			updateToCartObj = {
				cartId: cart.cartId,
				productCartId: cart.productCartId,
				user: userId,
				quantity: cart.quantity + 1,
				parentProduct: this.subProArray

			}
		}else{
			updateToCartObj = {
				cartId: cart.cartId,
				productCartId: cart.productCartId,
				quantity: cart.quantity + 1,
				product: cart.product
			}
		}

		//console.log(updateToCartObj);

		updateToCartObj = JSON.stringify(updateToCartObj)
			this.DataService.updateCartCount(updateToCartObj).subscribe(result => {
				if (result.statusCode == 200) {
					this.spinner.hide();
					this.ngOnInit();
					// this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
					// 	this.router.navigate(["cart"]));
				}
			})
	} else {
	  alert("You have exceeded the max quantity for this item");
	}

  }

	perCalculate(price, itemDiscount){
		var discount = (itemDiscount / price)*100
		return discount;
	}
}
