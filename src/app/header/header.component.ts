import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userId: any;
  isLoggedIn:any
  purchaseType : any;
  searchProductForm: FormGroup;
  submitted = false;
  message: any;
  products:any;
  favouriteCount:any;

  public User: User = new User({});
  constructor(
    private authService: AuthService,
    public router: Router,
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    private route: ActivatedRoute,

  ) { }

  ngOnInit() {
    this.userId = localStorage.getItem('userId');
    this.dataservice.subscribeNotification().subscribe(message =>{
      this.favouriteCount = message;
    })
    this.isLoggedIn = localStorage.getItem('isLoggedIn');
    let userId = localStorage.getItem('userId');
    if (this.isLoggedIn == 'true'){
      this.isLoggedIn = 'true';
    }else{
      this.isLoggedIn = 'false';
    }

    this.searchProductForm = this.formBuilder.group({
      search: ['']
    });

    if (localStorage.getItem('purchaseType')){
      this.purchaseType = localStorage.getItem('purchaseType');
    }else{
      this.purchaseType = 1;
    }
    this.favouriteCount = this.favouriteProduct(this.purchaseType,userId);
    this.sessionTime()

  }
  sessionTime(){
    this.isLoggedIn = localStorage.getItem('isLoggedIn');
    if (this.isLoggedIn == 'true') {
      if (localStorage.getItem("LoggedInTime")) {
        var t0 = Number(localStorage.getItem("LoggedInTime"));
        if (isNaN(t0)) t0 = 0;
        var t1 = new Date().getTime();
        var duration = t1 - t0;
        if (duration < 55 * 60 * 1000) {
          if (duration > 59 * 60 * 1000) {
            //this.refreshAuthToken();
            var loginTime = new Date().getTime();
            localStorage.setItem("LoggedInTime", loginTime.toString());
          }
        } else {
          alert("Your session has been expired, Please log in again.");
          this.logout();
        }
      }

    }
  }
  refreshAuthToken() {
    var token = this.dataservice.getCurrentUser().authToken;
    this.dataservice.refreshAuthToken(token, 'refreshAuthToken').subscribe(resultProfileData => {
      if (resultProfileData['statusCode'] == 200) {
        var authToken = resultProfileData['responsePacket'].authToken;
        localStorage.setItem('authtoken', authToken);
        this.getProfileData(authToken)
      }
    },
      error => {

      })
  }

  getProfileData(authToken) {
    this.dataservice.getUserProfile(this.userId).subscribe(resultProfileData => {
      if (resultProfileData['statusCode'] == 200) {
        this.User = new User(resultProfileData['responsePacket'])
        this.User.authToken = authToken;
        this.dataservice.setCurrentUser(this.User);
      }
    },
      error => {

      })
  }
  logout(): void {
    this.authService.logout();
    this.router.navigate(['']);
  }

  favouriteProduct(purchaseType:any,userId) {
      this.dataservice.getfavouriteProducts(purchaseType,userId).subscribe(resultFavouriteProducts => {
      if(resultFavouriteProducts.statusCode==200){
        this.products = resultFavouriteProducts.responsePacket;
        this.favouriteCount = this.products.length;
      }
    },
      error => {
        //(err) => console.log(err);
      })
  }



  get f() {

    return this.searchProductForm.controls;

  }
  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.searchProductForm.invalid) {
      return;
    }

    let searchString = this.f.search.value;

    if (this.f.search.value) {

      this.router.navigate(['/product','search'], { queryParams: { search: searchString } });
      //this.router.navigateByUrl('/profile');

    } else {
      alert('Please enter product name to search');
    }

  }
  userPurchaseType(id: any){
    localStorage.setItem('purchaseType', id);
    location.reload();
  }
}
