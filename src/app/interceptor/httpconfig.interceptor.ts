import { Injectable, ɵConsole } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AuthService } from 'src/app/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { DataService } from '../data.service'


@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
    helper: any = new JwtHelperService();
    constructor(
        public userService: DataService,
        private router: Router,
        private authService: AuthService,
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authToken: string = this.userService.getCurrentUser() ? this.userService.getCurrentUser().authToken : '';

        const isExpired = authToken ? this.helper.isTokenExpired(authToken) : false;

        if (isExpired && localStorage.getItem('isLoggedIn')) {

            //this.authService.logout();
            //this.router.navigate(['/login']);

        }

        if (request.method != "GET") {
            if (authToken) {



                if (!request.headers.has('Authorization')) {

                    request = request.clone({ headers: request.headers.set('Authorization', authToken) });


                }
            }

            if (!request.headers.has('Content-Type')) {
                //alert(2)
                request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
            }
        }
        //request = request.clone({ headers: request.headers.set('Accept', 'application/json') });

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    //console.log('event--->>>', event);

                }

                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                let data = {};
                data = {
                    reason: error && error.error.reason ? error.error.reason : '',
                    status: error.status
                };

                return throwError(error);
            }));
    }
}