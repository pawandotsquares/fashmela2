import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-success-payment',
  templateUrl: './success-payment.component.html',
  styleUrls: ['./success-payment.component.css']
})
export class SuccessPaymentComponent implements OnInit {
  transaction_id : any;
  paymentGatway : any;
  walletUser : any;
  flag : boolean;
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.flag = false;
    this.transaction_id = localStorage.getItem('transaction_id')
    this.paymentGatway = localStorage.getItem('paymentGatway');
    this.walletUser = localStorage.getItem('freecheckout')
   /*  setInterval(() => {
      this.battleInit();
    }, 10000); */
  }
  battleInit(){
    localStorage.setItem('transaction_id', '');
    localStorage.setItem('freecheckout', '');
    if (this.flag == false){
      this.flag = true;
      this.router.navigate(['/']);
    }
  }

}
