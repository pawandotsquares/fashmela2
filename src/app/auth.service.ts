import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  logout(): void {
    localStorage.removeItem('token');
    localStorage.setItem('isLoggedIn', "false");
    localStorage.removeItem('user_session_data');
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('addressId');
    localStorage.removeItem('email');
    localStorage.removeItem('admin_session_data');
    //localStorage.clear()
    /* localStorage.clear(); */
  }
}
