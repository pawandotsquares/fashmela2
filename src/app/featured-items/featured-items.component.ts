import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import {SliderModule} from 'primeng/slider';
import { myfavouriteItems } from '../util/helper';

@Component({
  selector: 'app-featured-items',
  templateUrl: './featured-items.component.html',
  styleUrls: ['./featured-items.component.css']
})
export class FeaturedItemsComponent implements OnInit {
  purchaseType: any;
  products: object;
  categoryList: object;
  trendingproduct: object;
  slides: object;
  slideConfig: object;
  priceType:object;
  sortType:any;
  sortedData:object;
  SortedProduct:object;
  discountfilter:object;
  userId:any;
  private status : boolean = true;
  sizes:object;
  sizevalue:object;
  discountvalue:object;
  val: number;
  rangeValues: number[] = [100,2000];
  actiontype:any;
  actionvalue:any;
  setType:number;

  p: number = 1;
    collection: any[];

    public spinnerConfig: any = {
      bdColor: 'rgba(51,51,51,0.8)',
      size: 'large',
      color: '#fff',
      type: 'ball-circus',
      loadigText: 'Loading...'
    };

  constructor(
    private dataservice: DataService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private Router: Router,
    private SliderModule: SliderModule
    ) {
    this.route.params.subscribe(params => this.getProductByPurchaseType(params['id']));
  }
  getProductByPurchaseType(id: any) {
    if (id && id == 1 || id == 2) {
      localStorage.setItem('purchaseType', id);
    }
  }

  ngOnInit() {
    this.spinner.show();
    this.userId = localStorage.getItem('userId');
    if (localStorage.getItem('purchaseType')){
      this.purchaseType = localStorage.getItem('purchaseType')
    }else{
      this.purchaseType = 1;
    }

    this.featuredProduct(this.purchaseType);
    //this.myFilterFunction(this.sortType);
    this.ProductSizes(this.purchaseType);
    //alert(this.rangeValues[0]);
  }


  // myfavouriteItems(productId,actionvalue){
  //   //alert(actionvalue);
  //   if(actionvalue == 0){
  //     this.actiontype = 'remove-favourite-product';
  //   }else{
  //     this.actiontype = 'add-favourite-product';
  //   }
  //   this.dataservice.addfavouriteProducts(productId,this.userId,this.actiontype).subscribe(resultFeaturedProduct => {
  //     this.spinner.hide();
  //     this.products = resultFeaturedProduct.responsePacket
  //     //console.log(this.products);
  //   },
  //     error => {

  //     })
  // }


  myfavouriteProducts(productId,actionvalue,idx){
    if (localStorage.getItem('userId')) {
      let userId = localStorage.getItem('userId');
    } else {
      this.Router.navigateByUrl('/login');
    }
    if(actionvalue){
      this.products[idx]['favorite']= true
    }else{
      this.products[idx]['favorite']= false
      this.featuredProduct(this.purchaseType);
    }
    myfavouriteItems(actionvalue, productId, this.userId, this.actiontype, this.spinner, this.dataservice, this.products);
  }


  featuredProduct(purchaseType:any) {
    this.status = !this.status;
    this.dataservice.getFeaturedProducts(purchaseType,this.userId).subscribe(resultFeaturedProduct => {
      this.spinner.hide();
      //console.log(resultFeaturedProduct)
      this.products = resultFeaturedProduct.responsePacket;

      //console.log(this.products);
    },
      error => {
        //(err) => console.log(err);
      })
  }

  myFilterFunction(sortType) {
    this.p = 1;
    if (sortType == 4){
      sortType = 0;
      var sortingData = {
        "productType": this.purchaseType,
        "categoryId": 0,
        "pageNumber": 1,
        "pageSize": 1000,
        "filterType": 0,
        "sortBy": sortType,
        "minValue": 0,
        "maxValue": 0,
        "sizeValue": "",
        "discount": 0,
        "userId": this.userId
      }
    }else{
       sortingData = {
        "productType": this.purchaseType,
        "categoryId": 0,
        "pageNumber": 1,
        "pageSize": 1000,
        "filterType": 0,
        "sortBy": sortType,
        "minValue": 0,
        "maxValue": 0,
        "sizeValue": "",
        "discount": 0,
        "userId": this.userId
      }
    }

     //console.log(sortingData);

    this.dataservice.sortFeaturedItem(sortingData).subscribe(resultSortedProduct => {
           this.spinner.hide();
           this.products = resultSortedProduct.responsePacket;
            //console.log(this.products);
           this.sortType = sortType;

         },
           error => {

    })
  }

  onChangeEvent(ev,filterType) {
    //console.log(ev);
    console.log(ev.target.value);
    if(filterType == 5){
      var discountvalue = parseInt(ev.target.value);
      var sizevalue = 0;
    }
    if(filterType == 2){

      sizevalue = parseInt(ev.target.value);
      discountvalue = 0;
    }

    if(filterType == 0){
      var sizevalue = 0;
      discountvalue = 0;
      filterType = parseInt(ev.target.value);
    }
    //console.log(ev);
   // console.log(discountvalue);
    var discountFilterData = {
      "productType": this.purchaseType,
      "categoryId": 0,
      "pageNumber": 0,
      "pageSize": 100,
      "filterType": filterType,
      "sortBy":0,
      "sizeValue": sizevalue,
      "minValue": 0,
      "maxValue": 0,
      "discount": discountvalue,
      "userId":this.userId
    }
    console.log(discountFilterData);
//return;
    this.dataservice.sortFeaturedItem(discountFilterData).subscribe(resultdiscountProduct => {
    this.spinner.hide();
    this.products = resultdiscountProduct.responsePacket;
    console.log(this.products);
    },
      error => {

    })
  }

  ProductSizes(purchaseType:any) {
    this.dataservice.getProductSizes(purchaseType).subscribe(resultProductSizes => {
    this.spinner.hide();
    this.sizes = resultProductSizes;
    },
    error => {
    })
  }

  handleChange() {
    this.ApplyFilters();
  }
  ApplyFilters(){
    //alert(this.rangeValues[0]);
    let FilterData = {
      "productType": this.purchaseType,
      "categoryId": 0,
      "pageNumber": 0,
      "pageSize": 10000,
      "filterType": 0,
      "sortBy": this.sortType,
      "minValue": this.rangeValues[0],
      "maxValue": this.rangeValues[1],
      "sizeValue": this.sizevalue,
      "discount": this.discountvalue,
      "userId":this.userId
    }
    //console.log(FilterData);
    this.dataservice.sortFeaturedItem(FilterData).subscribe(resultdiscountProduct => {
      this.spinner.hide();
      this.products = resultdiscountProduct.responsePacket;
      },
      error => {
      })
  }
}
