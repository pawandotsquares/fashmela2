import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { User } from 'src/app/models/user';
import { RequestOptions } from '@angular/http';
import { Observable, of, Subject } from 'rxjs';
import * as crypto from 'crypto-js';

 const apiUrl = 'https://fvyut8mcb2.execute-api.ap-south-1.amazonaws.com/pre-production';
const apiUrl2 = 'https://st9dk0u5o9.execute-api.ap-south-1.amazonaws.com/pre-production-next';

/*  const apiUrl = 'https://rebbu47ee5.execute-api.ap-south-1.amazonaws.com/uat';
const apiUrl2 = 'https://qmh3qmfp2j.execute-api.ap-south-1.amazonaws.com/uat-next';
 */
const shiprocket_service_url = 'https://apiv2.shiprocket.in/v1/external/auth/login';
const shiprocket_service_url2 = 'https://apiv2.shiprocket.in/v1/external/courier/track/shipment';
//https://apiv2.shiprocket.in/v1/external/courier/track/shipment/{shipment_id}
// const apiUrl = 'https://kwc47bogoc.execute-api.ap-south-1.amazonaws.com/production';
// const apiUrl2 = 'https://ad64rhtk3k.execute-api.ap-south-1.amazonaws.com/production-next';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
};
const secret = "@AKIAJSN26KWXJ45CJJRFUA@";
@Injectable({
  providedIn: 'root'
})
export class DataService {
  public notification = new Subject<any>();
  constructor(private http: HttpClient) { }


  updateNotification(notificationDetail: any) {
    this.notification.next(notificationDetail);
  }

  subscribeNotification(): Observable<any> {
    return this.notification.asObservable();
  }

  getFeaturedProducts(purchaseType,userId) {
    return this.http.post(apiUrl + '/get-featured-products', { productType: purchaseType,userId:userId }, httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  getTrendingProducts(purchaseType, userId) {
    return this.http.post(apiUrl + '/get-trending-products', { productType: purchaseType/* , userId: userId */}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getCategories() {
    return this.http.post(apiUrl +'/getcategorylist',{}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }


  userlogin(email, password) {
    return this.http.post(apiUrl+'/auth', { email: email,password: password }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  /* register(email, password) {
    return this.http.post(apiUrl+'/auth', { email: email, password: password }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  } */

  register(user, password) {
    return this.http.post(apiUrl+`/user-register`, {
      "firstName": user.name, "password": password, "email": user.email, "phoneNumber": user.mobile_number, "address": user.address, "userType": user.userType, "userReferCode": user.userReferCode}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  vendorRegister(user) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': 'registerVendor'
      })

    };

    return this.http.post(apiUrl2 + '/user-unauth', {
      "firstName": user.firstName, "email": user.email, "phoneNumber": user.phoneNumber, "companyName": user.companyName, "companyUrl": user.companyUrl, "panCardNumber": user.gstNumber, "gstNumber": user.panCardNumber
    }, httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  logout(): void {
    localStorage.setItem('isLoggedIn', "false");
    localStorage.removeItem('email');
  }

	getUserProfile(userId) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })

    };
    return this.http.post(apiUrl + `/get-user-profile`, { 'userId': userId }, httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
	));
}
	getBusinessProfile(userId) {
    return this.http.post(apiUrl +`/get-business-profile`, { 'userId': userId}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
	));
}
  updateUserProfile(user, base64Img) {
    return this.http.post(apiUrl+`/user-register`, {
      "userId": user.userId, "firstName": user.firstName, "password": user.password, "email": user.email, "phoneNumber": user.phoneNumber, "address": user.address, 'profileImage': base64Img
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  updateBusinessDetails(user, base64Img) {
    return this.http.post(apiUrl +`/save-business-profile`, {
      "businessProfileId": user.businessProfileId, "companyName": user.companyName, "holderName": user.holderName, "accountNumber": user.accountNumber, "ifscCode": user.ifscCode, "user": { "userId": user.userId, "isActive": false}
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  addBusinessDetails(userJson, user) {
    return this.http.post(apiUrl +`/save-business-profile`, {
      "businessProfileId": 0, "companyName": userJson.companyName, "holderName": userJson.holderName, "accountNumber": userJson.accountNumber, "ifscCode": userJson.ifscCode, user
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getProductList() {
    return this.http.post(apiUrl+`/getproductlistv1`, { "categoryId": 1, "productType": 1, "pageNumber": 1, "pageSize": 10, "searchKeyword": ""}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getProductListById(catId, purchaseType) {
    return this.http.post(apiUrl + `/getproductlistv1`, { "categoryId": catId, "productType": purchaseType, "pageNumber": 1, "pageSize": 10, "searchKeyword": "" }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getProductListBySearch(searchParam, purchaseType) {
    return this.http.post(apiUrl + `/getproductlistv1`, { "searchKeyword": searchParam, "productType": purchaseType, "pageNumber": 1, "pageSize": 10 }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  //, purchaseType
  //, "purchaseType": purchaseType
  getProductDetails(productId, purchaseType) {
    return this.http.post(apiUrl +`/getproductdetails`, { "productId": productId, "purchaseType": purchaseType }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  getProductDetailswithSize(purchaseType,productId,sizeId) {
    return this.http.post(apiUrl +`/getproductdetails`, { "productId": productId, "purchaseType": purchaseType,"sizeId":sizeId }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  forgotPass(emailId) {
    return this.http.post(apiUrl+`/forgot-password`, { "email": emailId }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  resetPass(token, newPassword) {
    return this.http.post(apiUrl+`/forgot-password`, { "token": token, "n": newPassword }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  checkMobile(isCheckMobileNumberRegistered, isEmailAsUserName, userName) {
    return this.http.post(apiUrl+`/change-password`, { "isCheckMobileNumberRegistered": isCheckMobileNumberRegistered, "isEmailAsUserName": isEmailAsUserName, "userName": userName }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  changePass(userName, oldPassword, newPassword) {
    return this.http.post(apiUrl+`/change-password`, { "userName": userName, "oldPassword": oldPassword, "newPassword": newPassword, "isEmailAsUserName":true }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  changePassByMobile(userName, newPassword) {
    return this.http.post(apiUrl + `/change-password`, { "userName": userName, "isAuthencatedWithMobileOtp": true, "newPassword": newPassword, "isEmailAsUserName": false }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  getUsersAddress(userId) {
    return this.http.post(apiUrl+`/get-delivery-address-list`, { "userId": userId}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getAddressById(deliveryAddressId) {
    return this.http.post(apiUrl + `/get-delivery-address`, { "deliveryAddressId": deliveryAddressId }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  adduserAddress(addressData, userId) {
    return this.http.post(apiUrl+`/save-user-delivery-address`, {
      "deliveryAddressId": addressData.deliveryAddressId, "fullName": addressData.name, "mobileNumber": addressData.mobile_number, "pincode": addressData.pin, "addressLine1": addressData.building, "addressLine2": addressData.locality, "city": addressData.city, "state": addressData.state, "fromName": addressData.fromName, "fromContactNumber": addressData.fromContactNumber, "fromAddress": addressData.fromAddress, "user": { "userId": userId }
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
 /*  deleteUserAddress(deliveryAddressId) {
    return this.http.post(apiUrl+`/delete-user-delivery-address`, {
      "deliveryAddressId": deliveryAddressId }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  } */
  deleteUserAddress(deliveryAddressId) {

    let body = JSON.stringify(
      {
        deliveryAddressId
      }
    );

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'my-auth-token'
      })
      ,
      body: body
    };

    return this.http.delete(apiUrl + `/delete-user-delivery-address`, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  sendContactQuery(user) {
    return this.http.post(apiUrl2 + `/save-user-feedback`, {
      "name": user.name, "email": user.email, "subject": user.subject, "message": user.message}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  walletAmount(userId) {
    return this.http.post(apiUrl2+`/get-user-wallet`, {
      "userId": userId}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  walletHistory(userId) {
    return this.http.post(apiUrl2 + `/get-user-wallet-history`, {
      "userId": userId}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  orderHistory(userId) {

   /*  const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type1': 'application/json',
        'Authorization1': 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJOb25lIiwicm9sZUlkIjoiMyIsImlzcyI6InN0cmVldHBpbiIsInRva2VuVHlwZSI6ImFjY2VzcyIsImV4cCI6MTU2NTAwODQ1NywidXNlcklkIjoiNyJ9.aUyQAg3P85QeO-V5f9b06lNa0mn3L0LnRJfkopWXurk'
      })

    }; */

    return this.http.post(apiUrl+`/order-history-v1`, {
      "userId": userId
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  orderDetails(orderUuId) {
    return this.http.post(apiUrl+`/get-order-details`, {
      "orderUuId": orderUuId
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  addToCart(data) {
    return this.http.post(apiUrl+`/add-to-cart`,
      data
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  proceedToCheckout(data){
    return this.http.post(apiUrl+`/proceed-to-checkout`,
      data
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getCart(userId, purchaseType) {
    return this.http.post(apiUrl+`/get-cart-items`, {
      "userId": userId, "purchaseType": purchaseType
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getCartCount(userId) {
    return this.http.post(apiUrl+`/get-cart-item-count`, {
      "userId": userId
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  updateCartCount(data) {
    return this.http.put(apiUrl+`/update-cart-item-quantity`, data).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  payNowData(data) {
    return this.http.post(apiUrl2 + `/proceed-payment`, data).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  deductWalletAmount(userId, walletamount) {

    return this.http.post(apiUrl2 + `/apply-user-wallet-amount`,{
      userId: userId, amount: walletamount, isRemove:true
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  palceOrder(data) {
    return this.http.post(apiUrl + `/place-order`, data).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  removeCartItem(productCartId) {

    let body = JSON.stringify(
      {
        productCartId
      }
    );

    const httpOptions = {
      headers: null
      ,
      body: body
    };

    return this.http.delete(apiUrl+`/remove-cart-item`, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  // sortFeaturedItem(sortingData, userId) {
  //   return this.http.post(apiUrl + '/get-featured-products', {
  //     "categoryId": sortingData.categoryId, "pageNumber": sortingData.pageNumber, "pageSize": sortingData.pageSize, productType: sortingData.productType, sortBy: sortingData.sortBy, 'discount': sortingData.discount, "sizeValue": sortingData.sizeValue, "minValue": sortingData.minValue, "maxValue": sortingData.maxValue, filterType: sortingData.filterType, userId: userId
  //   }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
  //   ));
  // }

   sortFeaturedItem(sortingData) {
    return this.http.post(apiUrl + '/get-featured-products', sortingData).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  sortTrendingItem(sortingData, userId) {
    return this.http.post(apiUrl + '/get-trending-products', {
      "categoryId": sortingData.categoryId, "pageNumber": sortingData.pageNumber, "pageSize": sortingData.pageSize, productType: sortingData.productType, sortBy: sortingData.sortBy, 'discount': sortingData.discount, "sizeValue": sortingData.sizeValue, "minValue": sortingData.minValue, "maxValue": sortingData.maxValue, filterType: sortingData.filterType, userId: userId
    }).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  /* sortTrendingItem(sortingData) {
    return this.http.post(apiUrl + '/get-trending-products', { sortingData: sortingData}).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  } */

  shiprocketAuth(){
    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      })

    };
    return this.http.post(shiprocket_service_url,{email:'wasimakram@dotsquares.com',password:'Dots@123'},httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }


  shiprocketTrackingData(token,shipment_id){

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Authorization':'Bearer '+ token,
        'Content-Type': 'application/json'
      })
    };
    return this.http.get(shiprocket_service_url2 + '/' + shipment_id,httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  getShipingChargesUsingWeight(token, addressData){

    const httpOptions = {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Authorization':'Bearer '+ token,
        'Content-Type': 'application/json'
      })
    };
    return this.http.get('https://apiv2.shiprocket.in/v1/external/courier/serviceability/?pickup_postcode=' + addressData.pickup_postcode + '&delivery_postcode=' + addressData.delivery_postcode + '&weight=' + addressData.weight + '&cod=' + addressData.cod,httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  getProductSizes(purchaseType){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action' : 'getHomeScreenData'
      })

    };

    return this.http.post(apiUrl2 + '/product',{productType: purchaseType},httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response.responsePacket.productSizeList))
    ));
  }

  applyCoupon(promoCode) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': 'checkCouponValidity'
      })

    };

    return this.http.post(apiUrl2 + '/order', { userId: promoCode.userId, couponCode: promoCode.couponCode }, httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  getfavouriteProducts(purchaseType,userId){

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action' : 'get-favourite-product'
      })

    };

    return this.http.post(apiUrl2 + '/product',{"productType": purchaseType, "pageNumber": 1, "pageSize": 10,"userId": userId},httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  addfavouriteProducts(productId,userId,actiontype){

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action' : actiontype
      })
    };
    return this.http.post(apiUrl2 + '/product',{"productId": productId,"userId": userId},httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  getParentProducts(categoryId,productType){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action' : 'get-parent-product-list'
      })
    };
    return this.http.post(apiUrl2 + '/product',{"productType":productType,"categoryId": categoryId, "pageNumber": 1, "pageSize": 10},httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  getSubProducts(parentProductId,productType,userId){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action' : 'get-subProduct-by-parentId'
      })
    };
    return this.http.post(apiUrl2 + '/product',{"productType":productType,"productId": parentProductId,"userId":userId},httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  getChatHistory(userId,actiontype){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action' : actiontype

      })
    };
    return this.http.post(apiUrl2 + '/user',{"userId":userId,"pageNumber": 1,"pageSize": 500000},httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }
  getEarnBonusData(userId, actiontype) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': actiontype

      })
    };
    return this.http.post(apiUrl2 + '/user', { "userId": userId }, httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  sendChatMessage(data,actiontype){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action' : actiontype

      })
    };
    return this.http.post(apiUrl2 + '/user',data,httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  returnOrder(actiontype,data) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action' : actiontype

      })
    };

    return this.http.post(apiUrl2+`/order`,data,httpOptions).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  refreshAuthToken(authToken, action) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'action': action
      }),

    };
    return this.http.post(apiUrl + `/user`, { authToken: authToken }, httpOptions,
    ).pipe(map((response: any) => JSON.parse(JSON.stringify(response))
    ));
  }

  setCurrentUser(currentUser: User): void {
    localStorage.setItem('vopersCurrentUser', this.encrypt(currentUser));
  }
  getCurrentUser(): User {
    let data = localStorage.getItem('vopersCurrentUser');
    if (data) {
      return new User(this.decrypt(data));
    }
    return null;

  }
  encrypt(data) {
    return crypto.AES.encrypt(JSON.stringify(data), secret,
      {
        keySize: 128 / 8,
        iv: secret,
        mode: crypto.mode.CBC,
        padding: crypto.pad.Pkcs7
      }).toString();
  }

  decrypt(data) {
    return JSON.parse(crypto.enc.Utf8.stringify(crypto.AES.decrypt(data, secret,
      {
        keySize: 128 / 8,
        iv: secret,
        mode: crypto.mode.CBC,
        padding: crypto.pad.Pkcs7
      })));
  }

}
