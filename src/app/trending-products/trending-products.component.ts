import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import {SliderModule} from 'primeng/slider';
import { myfavouriteItems } from '../util/helper';

@Component({
  selector: 'app-trending-products',
  templateUrl: './trending-products.component.html',
  styleUrls: ['./trending-products.component.css']
})
export class TrendingProductsComponent implements OnInit {
  clickMessage = '';
  purchaseType: any;
  products: object;
  categoryList: object;
  trendingproduct: object;
  slides: object;
  sortType:any;
  sizes:object;
  sizevalue:object;
  discountvalue:object;
  slideConfig: object;
  rangeValues: number[] = [20,80];
  val: number;
  actiontype:any;
  userId:any;
  actionvalue:any;
  p: number = 1;
  collection: any[];

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private dataservice: DataService,
    private route: ActivatedRoute,
    private Router: Router,
    private spinner: NgxSpinnerService,
    private SliderModule: SliderModule
    ) {
    this.route.params.subscribe(params => this.getProductByPurchaseType(params['id']));
  }
  getProductByPurchaseType(id: any) {
    if (id && id == 1 || id == 2) {
      localStorage.setItem('purchaseType', id);
    }
  }
  ngOnInit() {
    this.spinner.show();
    this.userId = localStorage.getItem('userId');
    if (localStorage.getItem('purchaseType')){
      this.purchaseType = localStorage.getItem('purchaseType')
    }else{
      this.purchaseType = 1;
    }

    this.trendingProducts(this.purchaseType)
    //console.log(this.trendingproduct);
  /* this.myFilterFunction(this.sortType);*/
    this.ProductSizes(this.purchaseType);

  }

  myfavouriteProducts(productId,actionvalue,idx){
    //alert(actionvalue);
    //debugger;
    //let userId = localStorage.getItem('userId');
    //console.log(this.trendingproduct[idx]['favorite']);

    if (localStorage.getItem('userId')) {
      let userId = localStorage.getItem('userId');
    } else {
      this.Router.navigateByUrl('/profile');
    }
    if(actionvalue){
      this.trendingproduct[idx]['favorite']= true
    }else{
      this.trendingproduct[idx]['favorite']= false
      this.trendingProducts(this.purchaseType);
    }
    myfavouriteItems(actionvalue, productId, this.userId, this.actiontype, this.spinner, this.dataservice, this.products);
  }

  trendingProducts(purchaseType: any) {
    this.dataservice.getTrendingProducts(purchaseType, this.userId).subscribe(resultTrendingProduct => {
      this.spinner.hide();
      this.trendingproduct = resultTrendingProduct.responsePacket
      console.log(this.trendingproduct);
    },
      error => {

      })
  }


  myFilterFunction(sortType) {
    //alert(this.purchaseType);
    if (sortType == 4) {
      sortType = 0;
      var sortingData = {
        "productType": this.purchaseType,
        "categoryId": 0,
        "pageNumber": 1,
        "pageSize": 1000,
        "filterType": 0,
        "sortBy": sortType,
        "minValue": 0,
        "maxValue": 0,
        "sizeValue": "",
        "discount": 0,
      }
    } else {
      sortingData = {
        "productType": this.purchaseType,
        "categoryId": 0,
        "pageNumber": 1,
        "pageSize": 1000,
        "filterType": 0,
        "sortBy": sortType,
        "minValue": 0,
        "maxValue": 0,
        "sizeValue": "",
        "discount": 0,
      }
    }
    /* let sortingData = {
       "productType": this.purchaseType,
       "categoryId": 0,
       "pageNumber": 1,
       "pageSize": 100,
       "filterType": 0,
       "sortBy": sortType,
       "minValue": 0,
       "maxValue": 0,
       "sizeValue": "",
       "discount": 0,
     } */

    this.dataservice.sortTrendingItem(sortingData, this.userId).subscribe(resultSortedProduct => {
      this.spinner.hide();
      this.trendingproduct = resultSortedProduct.responsePacket;
      //console.log(this.products);
      this.sortType = sortType;
     //  let products ={
     //    "products" : this.SortedProduct,
     //    "sortType" : sortType
     //  }

    },
      error => {

    })
}
//   myClickFunction() {
//     this.dataservice.getTrendingProducts(purchaseType).subscribe(resultTrendingProduct => {
//       this.spinner.hide();
//       this.trendingproduct = resultTrendingProduct.responsePacket
//       //console.log(this.trendingproduct[0].name);
//     },
//       error => {

//       })
//  }


onChangeEvent(ev,filterType) {
  if(filterType == 1){
    var discountvalue = parseInt(ev.target.value);
    var sizevalue = 0;
  }
  if(filterType == 2){

    sizevalue = parseInt(ev.target.value);
     discountvalue = 0;
  }
  let discountFilterData = {
    "productType": this.purchaseType,
    "categoryId": 0,
    "pageNumber": 0,
    "pageSize": 10000,
    "sizeValue": sizevalue,
    "filterType": 0,
    "sortBy": this.sortType,
    "minValue": 0,
    "maxValue": 0,
    "discount": this.discountvalue,
  }
  //console.log(discountFilterData);

  this.dataservice.sortTrendingItem(discountFilterData, this.userId).subscribe(resultdiscountProduct => {
  this.spinner.hide();
  this.trendingproduct = resultdiscountProduct.responsePacket;
  //console.log(this.products);
  },
    error => {

  })
}

ProductSizes(purchaseType:any) {
  this.dataservice.getProductSizes(purchaseType).subscribe(resultProductSizes => {
  this.spinner.hide();
  this.sizes = resultProductSizes;
  },
  error => {
  })
}

handleChange() {
  this.ApplyFilters();
}
ApplyFilters(){
  //alert(this.rangeValues[0]);

  let FilterData = {
    "productType": this.purchaseType,
    "categoryId": 0,
    "pageNumber": 0,
    "pageSize": 10000,
    "filterType": 0,
    "sortBy": this.sortType,
    "minValue": this.rangeValues[0],
    "maxValue": this.rangeValues[1],
    "sizeValue": this.sizevalue,
    "discount": this.discountvalue,
  }
  //console.log(FilterData);
  this.dataservice.sortTrendingItem(FilterData, this.userId).subscribe(resultdiscountProduct => {
    this.spinner.hide();
    this.trendingproduct = resultdiscountProduct.responsePacket;
    },
    error => {
    })
}
}
