import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-parent-product',
  templateUrl: './parent-product.component.html',
  styleUrls: ['./parent-product.component.css']
})
export class ParentProductComponent implements OnInit {
  purchaseType: any;
  categoryId:any;
  message: any;
  getparentproducts:object;
  noMsg: boolean;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  constructor(
    private dataservice: DataService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute
  ) { }
  
  ngOnInit() {
    this.spinner.show();
    this.route.params.subscribe(params => {
      this.categoryId = params['id'];
    });
    this.purchaseType = localStorage.getItem('purchaseType');
    this.showParentProducts(this.categoryId,this.purchaseType)
  }


  showParentProducts(categoryId,purchaseType) {
    this.dataservice.getParentProducts(categoryId,purchaseType).subscribe(resultparentproducts => {
      //console.log(resultparentproducts);
      this.spinner.hide();
      this.noMsg = true;
			if(resultparentproducts.statusCode == 200) {
        this.getparentproducts = resultparentproducts.responsePacket;
      
        }else {
          this.message = resultparentproducts;
        }
      },
      error => {

      })
  }

}
