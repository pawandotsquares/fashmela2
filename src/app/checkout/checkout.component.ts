import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
	selector: 'app-checkout',
	templateUrl: './checkout.component.html',
	styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
	public payuform: any = {};
	htmlOptions: any = {};
	couponDiscountValue: any = {};
	couponID: any;
	promoCode: any = {};
	shipment_token: any = {};
	couponData: any;
	paymentForm: FormGroup;
	cartData: any;
	message: object;
	addressData: object;
	userId: any;
	purchaseType: any;
	quantity: any;
	maxQuantity: any;
	paynow: boolean;
	paymentGatway: any;
	cartProductData: any;
	finalPriceValue: number;
	mainFinalPriceValue: number;
	cartMainFinalPriceValue: number;
	totalShippingCharge: any;
	specialDiscountValue: any;
	finalPrice: number;
	finaMrpValue: number;
	cartDataCount: number;
	discount: number;
	discountPercent: any;
	deliveryAddress: boolean;
	noDeliveryAddress: boolean;
	walletData: any;
	walletSelected: any;
	walletTotalAmount: number;
	walletDiducdedAmount: number;
	walletFlag: boolean;
	isCheckedVal: boolean;
	finalData: any;
	submitted = false;
	sizeList = [];
	subProducts = [];
	orderArray = [];
	sizeArray = [];
	colorImageArray = [];
	proceedToCheckoutObj: any;
	orderItems = [];
	subProArray: object;
	checkoutData: any;
	weight: number;
	isHidden : boolean = true
	public spinnerConfig: any = {
		bdColor: 'rgba(51,51,51,0.8)',
		size: 'large',
		color: '#fff',
		type: 'ball-circus',
		loadigText: 'Loading...'
	};
	constructor(
		private DataService: DataService,
		private spinner: NgxSpinnerService,
		private router: Router,
		private http: Http,
		private formBuilder: FormBuilder
	) { }

	ngOnInit() {
		this.couponID = localStorage.getItem('couponID');
		this.couponDiscountValue = localStorage.getItem('couponDiscountValue');
		this.totalShippingCharge = parseInt(localStorage.getItem('shipingCharges'));
		this.specialDiscountValue = parseInt(localStorage.getItem('specialDiscountValue'));
		this.walletTotalAmount = 0;
		this.walletDiducdedAmount = 0;
		this.noDeliveryAddress = false
		this.spinner.show();
		this.userId = localStorage.getItem('userId');
		this.walletAmount(this.userId)
		this.purchaseType = localStorage.getItem('purchaseType');
		this.cartList(this.userId, this.purchaseType);
		this.cartCount(this.userId);
		const deliveryAddressId = localStorage.getItem('addressId')

		if (deliveryAddressId) {
			this.getAddressById(deliveryAddressId)
		} else {
			this.allAddress(this.userId)
		}

	}
	getShipingChargesUsingWeight(pincode) {
		this.DataService.shiprocketAuth().subscribe(resultshiprocketAuth => {
			var addressData = {
				pickup_postcode: 302019,
				delivery_postcode: pincode,
				weight: this.weight,
				cod: 0
			}

			this.shipment_token = resultshiprocketAuth.token;

			if (this.shipment_token) {
				this.DataService.getShipingChargesUsingWeight(this.shipment_token, addressData).subscribe(resultshiprocketTracking => {
					if (resultshiprocketTracking.status == 200) {
						this.totalShippingCharge = resultshiprocketTracking['data']['available_courier_companies'][0]['rate'];
						console.log(this.weight);
						console.log(resultshiprocketTracking['data']);

						localStorage.setItem('shipingCharges', this.totalShippingCharge);
					} else {

					}

				},
					error => {

					})

			}
		},
			error => {

			})
	}
	cartList(userId, purchaseType) {

		this.spinner.show();
		this.DataService.getCart(userId, purchaseType).subscribe(resultCartData => {
			let proceedtoCheckoutObj = {};

			if (resultCartData.statusCode == 200) {

				this.weight = resultCartData.responsePacket.totalWeight;
				if (this.weight){
					this.weight = this.weight/1000
					}
				this.cartDataCount = resultCartData.responsePacket.totalCartItemCount;
				this.finaMrpValue = 0;
				this.finalPriceValue = 0;
				resultCartData.responsePacket.cartItems.map(obj => {
					this.finalPriceValue = this.finalPriceValue + obj.cartItemTotalPrice;
					this.finaMrpValue = 0;
				});
				this.finaMrpValue = this.finalPriceValue;
				if (this.specialDiscountValue > 0 && this.finalPriceValue >= this.specialDiscountValue) {

					this.finalPriceValue = this.finalPriceValue - this.specialDiscountValue;
				}

				if (this.couponDiscountValue) {
					this.finalPriceValue = this.finalPriceValue - this.couponDiscountValue;
				}
				this.mainFinalPriceValue = this.finalPriceValue;
				this.cartMainFinalPriceValue = this.finalPriceValue;

				this.cartData = resultCartData.responsePacket.cartItems
				this.userId = {
					userId: this.userId
				}
				let proId = {};
				let productPurchaseType = {};

				for (let orderitems of this.cartData) {
					if (orderitems.parentProduct) {
						let proId = orderitems.parentProduct.productId;
						let productPurchaseType = 3;
						this.sizeArray = [
							{
								sizeId: orderitems.parentProduct.subProducts[0].sizeList[0].sizeId
							}
						]

						for (let orderSubProducts of orderitems.parentProduct.subProducts) {

							this.subProducts.push({
								productId: orderSubProducts.productId
							})
						}

						this.orderArray.push({
							productId: proId,
							quantity: orderitems.quantity,
							productType: productPurchaseType,
							sizeList: this.sizeArray,
							subProducts: this.subProducts
						})

					} else {
						let proId = orderitems.product.productId;
						let productPurchaseType = orderitems.product.purchaseType
						this.sizeArray = orderitems.product.sizeList

						this.orderArray.push({
							productId: proId,
							quantity: orderitems.quantity,
							productType: productPurchaseType,
							sizeList: this.sizeArray,
							subProducts: null
						})
					}
				}

				this.proceedToCheckoutObj = {
					orderId: 0,
					orderItems: this.orderArray,
					user: this.userId
				}

				proceedtoCheckoutObj = JSON.stringify(this.proceedToCheckoutObj);

				this.DataService.proceedToCheckout(proceedtoCheckoutObj).subscribe(resultcheckout => {
					this.spinner.hide();

					if (resultcheckout.statusCode == 200) {
						this.isHidden = false
						this.checkoutData = resultcheckout.responsePacket

						if (this.checkoutData.orderPlaced == true) {
							this.cartData.forEach(parentProduct => {

								if (parentProduct.parentProduct) {

									this.sizeList = [
										{
											sizeId: parentProduct.parentProduct.subProducts[0].sizeList[0].sizeId
										}
									]
									for (let childProdId of parentProduct.parentProduct.subProducts) {
										this.subProducts.push({
											productId: childProdId.productId,
										})
									}

									this.subProArray = {
										subProducts: this.subProducts
									}
									if (parentProduct.itemDiscount) {
										var fPrice = parentProduct.parentProduct.cartItemTotalPrice
									} else {
										fPrice = parentProduct.parentProduct.cartItemTotalPrice
									}
									let product = {
										"productId": parentProduct.parentProduct.productId,
										"totalPrice": fPrice,
										"shippingCharge": 0,
										"courierPartner": "rocket mail",
										"quantity": parentProduct.parentProduct.quantity,
										"productType": 3,
										"itemDiscount": parentProduct.itemDiscount,
										"subProducts": this.subProArray,
										"sizeList": this.sizeList
									}
									this.orderItems.push(product);
								} else {
									if (parentProduct.itemDiscount) {
										var fPrice = parentProduct.cartItemTotalPrice
									} else {
										fPrice = parentProduct.cartItemTotalPrice
									}
									let product = {
										"productId": parentProduct.product.productId,
										"totalPrice": fPrice,
										"shippingCharge": 0,
										"courierPartner": "rocket mail",
										"quantity": parentProduct.quantity,
										"productType": parentProduct.product.purchaseType,
										"itemDiscount": parentProduct.itemDiscount,
										"sizeList": parentProduct.product.sizeList,
									}
									this.orderItems.push(product);
								}

							});
							this.cartProductData = this.orderItems;

						} else {
							alert(resultcheckout.message);
						}
					} else {
						alert(resultcheckout.message);
					}
				})

			} else if (resultCartData.statusCode == 202) {
				alert("Your account has been deactivated, Please login again to proceed");
				this.router.navigateByUrl("/login");
			} else {
				this.router.navigate(["cart"]);
				this.message = resultCartData.message;
			}
		}

		)
	}

	perCalculate(price, itemDiscount) {
		var discount = (itemDiscount / price) * 100
		return discount;
	}

	cartCount(userId) {
		this.spinner.show();
		this.DataService.getCartCount(userId).subscribe(resultCartCount => {
			this.spinner.hide();

			if (resultCartCount.statusCode == 200) {

				this.mainFinalPriceValue = resultCartCount.responsePacket.total_price;

				this.discountPercent = resultCartCount.responsePacket.totalDiscountPercentage;

			} else {
				this.message = resultCartCount.message;
			}
		})
	}
	walletAmount(userId) {
		this.walletFlag = false;

		this.DataService.walletAmount(userId).subscribe(resultWalletData => {
			if (resultWalletData.statusCode == 200) {
				this.walletData = resultWalletData.responsePacket
				this.walletTotalAmount = resultWalletData.responsePacket.amount
				if (this.walletTotalAmount > 0) {
					this.walletFlag = true;
				}
			} else {
				this.message = resultWalletData;
				localStorage.setItem('statusCode', resultWalletData.message);
			}
		},
			error => {

			})
	}
	allAddress(userId) {
		this.DataService.getUsersAddress(userId).subscribe(allAddressData => {
			this.spinner.hide();
			if (allAddressData.statusCode == 200) {
				console.log(allAddressData)
				localStorage.setItem('addressId', allAddressData.responsePacket[0].deliveryAddressId)
				var pincode = allAddressData.responsePacket[0].pincode
				this.addressData = allAddressData.responsePacket
				if (this.purchaseType != 1) {

					this.getShipingChargesUsingWeight(pincode);
				}
				this.deliveryAddress = false;
				this.noDeliveryAddress = true;
			} else {
				this.noDeliveryAddress = false;
				this.message = allAddressData;
				localStorage.setItem('statusCode', allAddressData.message);
			}
		},
			error => {

			})
	}
	getAddressById(addressId) {
		this.DataService.getAddressById(addressId).subscribe(resultAddressData => {
			this.spinner.hide();
			if (resultAddressData.statusCode == 200) {
				console.log(resultAddressData);
				localStorage.setItem('addressId', resultAddressData.responsePacket.deliveryAddressId)
				this.addressData = resultAddressData.responsePacket
				var pincode = resultAddressData.responsePacket.pincode
				this.deliveryAddress = true;
				this.noDeliveryAddress = true;
				if (this.purchaseType != 1) {
					this.getShipingChargesUsingWeight(pincode);
				}
			} else {
				this.message = resultAddressData;
				localStorage.setItem('statusCode', resultAddressData.message);
			}
		},
			error => {

			})
	}
	checkValue(event: any) {

		if (event == "A") {
			localStorage.setItem("walletSelected", 'true');
			this.isCheckedVal = true;
		} else {
			localStorage.setItem("walletSelected", 'false');
			this.isCheckedVal = false;
		}

		if (localStorage.getItem("walletSelected") == "true") {
			this.walletDiducdedAmount = 0
			if (this.walletTotalAmount < 100) {
				if (this.walletTotalAmount < this.mainFinalPriceValue) {
					this.finalPrice = this.finalPriceValue - this.walletTotalAmount;
					this.walletDiducdedAmount = this.walletTotalAmount;
				} else {
					this.finalPrice = 0;
					this.walletDiducdedAmount = this.mainFinalPriceValue;
				}
			} else {
				if (this.mainFinalPriceValue > 100) {
					this.walletDiducdedAmount = 100;
					this.finalPrice = this.finalPriceValue - this.walletDiducdedAmount;
				} else {
					this.finalPrice = 0;
					this.walletDiducdedAmount = this.mainFinalPriceValue;
				}
			}
		} else {
			this.finalPrice = this.mainFinalPriceValue;
			this.walletDiducdedAmount = 0;
		}

		this.finalPriceValue = this.finalPrice;

	}

	payNow() {
		//alert(this.mainFinalPriceValue);
		if (this.walletDiducdedAmount) {
			this.tempWalletAmount(this.userId, this.walletDiducdedAmount);
		}
		const paymentData = {
			amount: this.finalPriceValue + this.totalShippingCharge,
			walletDiducdedAmount: this.walletDiducdedAmount,
			mainFinalPriceValue: this.mainFinalPriceValue,
		}
		localStorage.setItem('finalPaymentData', JSON.stringify(paymentData));
		let addressId = localStorage.getItem('addressId');
		if (this.noDeliveryAddress) {
			if (this.finalPriceValue > 0) {
				this.router.navigate(["payment"]);
			} else {
				let palceOrderObj = {};

				palceOrderObj = {
					orderItems: this.cartProductData,
					user: {
						userId: localStorage.getItem('userId')
					},
					payment: {
						modeOfPayment: "",
						successUrl: "https://d3rc17ccvsmeqb.cloudfront.net/payment-success",
						failureUrl: "https://d3rc17ccvsmeqb.cloudfront.net/payment-failure",
						paymentGatway: "",
					},
					totalAmount: this.mainFinalPriceValue, // total without discount and wallet
					paidAmount: this.finalPriceValue, //paid on payment gatway
					walletAmountUsed: this.walletDiducdedAmount, // used from wallet
					couponId: this.couponID, // Coupon ID
					CouponDiscountAmount: this.couponDiscountValue, // Coupon Discount Amount
					ShippingAmount: this.totalShippingCharge, // Shipping Amount
					address: {
						deliveryAddressId: localStorage.getItem('addressId'),
					},
				};


				palceOrderObj = JSON.stringify(palceOrderObj);

				//return;
				this.DataService.palceOrder(palceOrderObj).subscribe(resultPaymentData => {
					if (resultPaymentData.statusCode == 200) {

						this.finalData = resultPaymentData.responsePacket
						localStorage.setItem('transaction_id', this.finalData.orderUuId)
						localStorage.setItem('freecheckout', "true"),
							this.router.navigate(["payment-success"]);
					} else if (resultPaymentData.statusCode == 202) {
						localStorage.clear()
						alert("Your account has been deactivated, Please login again to proceed");
						this.router.navigateByUrl("/login");
					} else {
						localStorage.setItem('freecheckout', "false"),
							this.router.navigate(["payment-failure"]);
					}
				},
					error => {
					})
			}
		} else {
			alert("Please select address");
		}

	}
	tempWalletAmount(userId, walletamount) {
		this.DataService.deductWalletAmount(userId, walletamount).subscribe(resultWalData => {
			if (resultWalData.statusCode == 200) {
			} else {

			}
		},
			error => {
			})
	}

}
