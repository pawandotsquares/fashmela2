import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  paytmPay: boolean;
  payUoneyPay: boolean;
  buttonPay: boolean;
  finalData: any;
  userId: any;
  paynow: boolean;
  purchaseType: any;
  cartData: any;
  cartProductData: any;
  message: object;
  trancation_id: any;
  order_id: any;
  emptyMessage: string;
  converted: any;
  paymentGatway:any;
  disablePaymentButton: boolean = true;
  sizeList=[];
  subProducts=[];
  subProArray:object;
  couponDiscountValue: any = {};
  couponID: any;
  totalShippingCharge: any;

  public finalPaymentData: any = {};
  public payuform: any = {};
  public paytm: any = {};
  constructor(
    private http: HttpClient,
    private DataService : DataService,
    private router : Router
    ) { }

  ngOnInit() {
    this.couponID = localStorage.getItem('couponID');
    this.couponDiscountValue = localStorage.getItem('couponDiscountValue');
    this.totalShippingCharge = parseInt(localStorage.getItem('shipingCharges'));
    this.userId = localStorage.getItem('userId');
    this.purchaseType = localStorage.getItem('purchaseType');
    this.payUoneyPay = true;
    this.paytmPay = true;
    this.buttonPay = true;
    this.paynow = false;
    this.cartList(this.userId, this.purchaseType);
  }
  cartList(userId, purchaseType) {
    this.DataService.getCart(userId, purchaseType).subscribe(resultCartData => {
      var orderItems = [];
      if (resultCartData.statusCode == 200) {
        this.cartData = resultCartData.responsePacket.cartItems;
        console.log(this.cartData);
        this.cartData.forEach(parentProduct =>{

          if(parentProduct.parentProduct){

              this.sizeList = [
                {
                  sizeId: parentProduct.parentProduct.subProducts[0].sizeList[0].sizeId
                }
              ]
              for (let childProdId of parentProduct.parentProduct.subProducts) {
                this.subProducts.push({
                    productId: childProdId.productId,
                 })
              }

            if (parentProduct.itemDiscount) {
              var fPrice = parentProduct.parentProduct.cartItemTotalPrice
            } else {
              fPrice = parentProduct.parentProduct.cartItemTotalPrice
            }
              let product = {
                "productId": parentProduct.parentProduct.productId,
                "totalPrice": fPrice,
                "shippingCharge": 0,
                "courierPartner": "rocket mail",
                "quantity": parentProduct.quantity,
                "productType": 3,
                "itemDiscount":parentProduct.parentProduct.itemDiscount,
                "subProducts": this.subProducts,
                "sizeList": this.sizeList
              }
              orderItems.push(product);
            }else{
            if (parentProduct.itemDiscount) {
              var fPrice = parentProduct.cartItemTotalPrice
            } else {
              fPrice = parentProduct.cartItemTotalPrice
            }
              let product = {
                "productId": parentProduct.product.productId,
                "totalPrice": fPrice,
                "shippingCharge": 0,
                "courierPartner": "rocket mail",
                "quantity": parentProduct.quantity,
                "productType": parentProduct.product.purchaseType,
                "itemDiscount": parentProduct.itemDiscount,
                "sizeList": parentProduct.product.sizeList,
              }
              orderItems.push(product);
            }

          });
        this.cartProductData = orderItems;
        console.log('cartProductData');
        console.log(this.cartProductData);
      } else {
        this.message = resultCartData.message;
        this.emptyMessage = "Your Shopping Cart is empty";
      }
    }
    )
  }
  isFloat_number(v) {
    var num = /^[-+]?[0-9]+\.[0-9]+$/;
    return num.test(v);
  }
  onSelectionChange(entry) {
    localStorage.setItem('transaction_id', '');
    localStorage.setItem('freecheckout', '');
    if (entry == 1) {
      this.payUoneyPay = false;
      this.paytmPay = true;
      this.paymentGatway = "paytm"
    } else {
      this.payUoneyPay = true;
      this.paytmPay = false;
      this.paymentGatway = "payumoney"
    }
    localStorage.setItem('paymentGatway', this.paymentGatway)
    this.buttonPay = false;
    this.finalPaymentData = JSON.parse(localStorage.getItem('finalPaymentData'));

    let palceOrderObj = {};

     palceOrderObj = {
      orderItems: this.cartProductData,
      user: {
        userId: localStorage.getItem('userId')
      },
      payment: {
        modeOfPayment: this.paymentGatway,
        successUrl: "https://kn86qvt0k2.execute-api.ap-south-1.amazonaws.com/dev/paymentgatewaysuccess",
        failureUrl:"https://kn86qvt0k2.execute-api.ap-south-1.amazonaws.com/dev/paymentgatewaysuccess",
        paymentGatway: this.paymentGatway,
      },
       /* totalAmount: this.finalPaymentData.mainFinalPriceValue,
       walletAmountUsed: this.finalPaymentData.walletDiducdedAmount,
       paidAmount: this.finalPaymentData.amount, */

       totalAmount: this.finalPaymentData.mainFinalPriceValue, // total without discount and wallet
       paidAmount: this.finalPaymentData.amount, //paid on payment gatway
       walletAmountUsed: this.finalPaymentData.walletDiducdedAmount, // used from wallet
       couponId: this.couponID, // Coupon ID
       CouponDiscountAmount: this.couponDiscountValue, // Coupon Discount Amount
       ShippingAmount: this.totalShippingCharge, // Shipping Amount


       address: {
         deliveryAddressId: localStorage.getItem('addressId'),
       },
    };
    console.log('palceOrderObj');
    console.log(palceOrderObj);
    palceOrderObj = JSON.stringify(palceOrderObj)

    this.DataService.palceOrder(palceOrderObj).subscribe(resultPaymentData => {

      if (resultPaymentData.statusCode == 200) {

        if (resultPaymentData.responsePacket.payment.paymentGatway == "payumoney"){
          this.finalData = resultPaymentData.responsePacket
          localStorage.setItem('transaction_id', this.finalData.orderUuId)
          this.converted = parseFloat(this.finalData.paidAmount).toFixed(2)

          this.payuform.productinfo = this.finalData.payment.productinfo;
          this.payuform.firstname = this.finalData.user.firstName;
          this.payuform.email = this.finalData.user.email;
          this.payuform.phone = this.finalData.user.phoneNumber;
          this.payuform.amount = this.converted;
          this.payuform.surl = this.finalData.payment.successUrl;
          this.payuform.furl = this.finalData.payment.failureUrl;
          this.payuform.key = this.finalData.payment.key;
          this.payuform.hash = this.finalData.payment.hash;
          this.payuform.txnid = this.finalData.orderUuId;
          this.paynow = true;
          setTimeout(() => {
            document.getElementById("payuSubmit").click();
          })

        }
        if (resultPaymentData.responsePacket.payment.paymentGatway == "paytm") {
          this.finalData = resultPaymentData.responsePacket
          localStorage.setItem('transaction_id', this.finalData.orderUuId)
          this.converted = parseFloat(this.finalData.paidAmount).toFixed(2)
        //  this.paytm.mid = 'PKMZUx43066293325548'; //custome chhanges
          this.paytm.mid = 'muZJdf09382331747582'; //custome chhanges
          this.paytm.website = 'DEFAULT';
          this.paytm.indtyp = 'Retail';
          this.paytm.channelId = 'WEB';
          this.paytm.userId = this.finalData.user.userId;
          this.paytm.email = this.finalData.user.email;
          this.paytm.phone = this.finalData.user.phoneNumber;
          this.paytm.amount = this.converted;
          this.paytm.surl = this.finalData.payment.successUrl;
          this.paytm.hash = this.finalData.payment.hash;
          this.paytm.txnid = this.finalData.orderUuId;
          this.paynow = true;
          setTimeout(() => {
            document.getElementById("paytmSubmit").click();
          })
        }
      } else {
        alert(resultPaymentData.message);
        //this.message = resultCartData.message;
      }
    },
      error => {
      })

  }

  onSelectionChange1(entry) {
    this.buttonPay = false;
    if (entry == 1){
      this.payUoneyPay = false;
      this.paytmPay = true;
      this.paymentGatway = "paytm"
      this.payNowByPaytm(this.paymentGatway)
    }else{
      this.payUoneyPay = true;
      this.paytmPay = false;
      this.paymentGatway = "payumoney"
      this.payNowByPayUMoney(this.paymentGatway)
    }
  }
  payNowByPayUMoney(paymentGatway) {
    let finalPaymentData = JSON.parse(localStorage.getItem('finalPaymentData'));
    const paymentPayload = {
      userId: this.userId,
      email: "pawankumar.joshi@dotsquares.com",
      name: "Pawan",
      phone: "9929198620",
      paymentGatway: "payumoney",
      productInfo: "vopers",
      sUrl: "https://kn86qvt0k2.execute-api.ap-south-1.amazonaws.com/dev/paymentgatewaysuccess",
      fUrl: "https://kn86qvt0k2.execute-api.ap-south-1.amazonaws.com/dev/paymentgatewaysuccess",
      amount: 444.5
      /* amount: finalPaymentData.amount */
    }
    this.DataService.payNowData(paymentPayload).subscribe(resultPaymentData => {

      if (resultPaymentData.statusCode == 200) {
        this.finalData = resultPaymentData.responsePacket
        this.payuform.productinfo = this.finalData.productInfo;
        this.payuform.firstname = this.finalData.name;
        this.payuform.email = this.finalData.email;
        this.payuform.phone = this.finalData.phone;
        this.payuform.amount = this.finalData.amount;
        this.payuform.surl = this.finalData.sUrl;
        this.payuform.furl = this.finalData.fUrl;
        this.payuform.key = this.finalData.key;
        this.payuform.hash = this.finalData.hash;
        this.payuform.txnid = this.finalData.txnId;
        this.payuform.service_provider = this.finalData.service_provider;
        this.paynow = true;
      } else {

      }
    },
      error => {
      })
  }
  payNowByPaytm(paymentGatway) {
    let finalPaymentData = JSON.parse(localStorage.getItem('finalPaymentData'));
    const paymentPayload = {
      userId: this.userId,
      email: "pawankumar.joshi@dotsquares.com",
      name: "Pawan",
      phone: "9929198620",
      paymentGatway: paymentGatway,
      productInfo: "vopers",
      sUrl: "http://dev.fashmela.s3-website.ap-south-1.amazonaws.com/payment-success",
      fUrl: "http://dev.fashmela.s3-website.ap-south-1.amazonaws.com/payment-success",
      amount: finalPaymentData.amount
    }
    this.DataService.payNowData(paymentPayload).subscribe(resultPaymentData => {
      if (resultPaymentData.statusCode == 200) {
        this.finalData = resultPaymentData.responsePacket
        this.paytm.mid = 'muZJdf09382331747582';
        this.paytm.website = 'WEBSTAGING';
        this.paytm.indtyp = 'Retail';
        this.paytm.channelId = 'WEB';
        this.paytm.userId = this.finalData.userId;
        this.paytm.email = this.finalData.email;
        this.paytm.phone = this.finalData.phone;
        this.paytm.amount = this.finalData.amount;
        this.paytm.surl = this.finalData.sUrl;
        this.paytm.hash = this.finalData.hash;
        this.paytm.txnid = this.finalData.txnId;
        this.paynow = true;
      } else {

      }
    },
      error => {
      })
  }

}