export class User {

    firstName: any;
    email: any;
    phoneNumber: any;
    address: any;
    userType: any;
    userReferCode: any;
    authToken: any;


    constructor(params: any = {}) {
        Object.assign(this, params);
    }

}