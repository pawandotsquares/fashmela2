export class Product {
    
    id: number;
    product_name: string;
    product_price: string;

    constructor(params: any = {}) {
        Object.assign(this, params);
    }

}