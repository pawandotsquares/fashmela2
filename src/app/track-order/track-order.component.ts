import { Component, OnInit } from '@angular/core';
import { DataService } from "../data.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-track-order',
  templateUrl: './track-order.component.html',
  styleUrls: ['./track-order.component.css']
})
export class TrackOrderComponent implements OnInit {
  orderId: any;
  shipment_id:any;
  shipment_token:any;
  tracking:any;
  noMsg: boolean;
  constructor(
    private dataservice: DataService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
     this.route.params.subscribe(params => {
       this.orderId = params['id'];
     });
     this.trackOrder(this.orderId)
     this.noMsg = false;
    //alert(this.orderId);
  }

  trackOrder(orderId: any) {
		this.dataservice.orderDetails(orderId).subscribe(resultOrderShipping => {
      //console.log(resultOrderShipping);
      
      this.shipment_id = resultOrderShipping.responsePacket.shipmentId;
      this.spinner.hide();
      this.noMsg = true;
			if(resultOrderShipping.statusCode == 200) {
        //this.products = resultFeaturedProduct.responsePacket
        this.dataservice.shiprocketAuth().subscribe(resultshiprocketAuth => {
          
          this.shipment_token = resultshiprocketAuth.token;
          //console.log(this.shipment_token);
          if(this.shipment_token) {
           
            this.dataservice.shiprocketTrackingData(this.shipment_token,'10711905').subscribe(resultshiprocketTracking => {
              //console.log(resultshiprocketTracking);
              if (resultshiprocketTracking.tracking_data.shipment_status) {
                   this.tracking = resultshiprocketTracking.tracking_data.shipment_track_activities;  
              }
            },
            error => {
  
            })

          }
        },
        error => {

        })

        }
    },
      error => {

      })
  }


}
