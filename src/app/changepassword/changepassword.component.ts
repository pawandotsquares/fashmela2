import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { Md5 } from 'ts-md5/dist/md5';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  changePassForm: FormGroup;
  submitted = false;
  message: any;
  confirmPassNotValid: boolean;
	token: any;

	public spinnerConfig: any = {
		bdColor: 'rgba(51,51,51,0.8)',
		size: 'large',
		color: '#fff',
		type: 'ball-circus',
		loadigText: 'Loading...'
	};


  constructor(
		private formBuilder: FormBuilder,
		private dataservice: DataService,
		private router: Router,
		private route: ActivatedRoute,
		private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
	this.changePassForm = this.formBuilder.group({
	  old_password: ['', Validators.required],
	  password: ['', [Validators.required, Validators.minLength(6)]],
	  confirm_password: ['', Validators.required]
	}, { validator: this.checkIfMatchingPasswords('password', 'confirm_password') });
  }
  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {

	return (group: FormGroup) => {
	  let passwordInput = group.controls[passwordKey],
		passwordConfirmationInput = group.controls[passwordConfirmationKey];
		if (passwordConfirmationInput.value) {
			if (passwordInput.value !== passwordConfirmationInput.value) {
				return passwordConfirmationInput.setErrors({ notEquivalent: true })
			}else {
				return passwordConfirmationInput.setErrors(null);
			}
		}
	}
  }
  // convenience getter for easy access to form fields
  get f() {

	return this.changePassForm.controls;

  }
  onSubmit() {

	this.submitted = true;

	if (this.f.password.value != this.f.confirm_password.value) {
	  this.confirmPassNotValid = true;
	  return;
	} else {
	  this.confirmPassNotValid = false;
	}

	// stop here if form is invalid
	if (this.changePassForm.invalid) {
	  return;
	}
		this.spinner.show();
	if (this.f.password.value) {

	  const md5 = new Md5();
	  let oldPassword = md5.appendStr(this.f.old_password.value).end();
	  const md51 = new Md5();
	  let newPassword = md51.appendStr(this.f.password.value).end();
	  let email = localStorage.getItem('email');
	  //call login api here
		this.dataservice.changePass(email, oldPassword, newPassword).subscribe(result => {
			this.spinner.hide();
		if (result.statusCode == 200) {
		  this.message = result;
		  this.router.navigateByUrl('/profile');
		} else {
		  this.message = result;
		}

	  },
		error => {

		})
	} else {
	  alert('Error Occurred.');
	}
  }

}
