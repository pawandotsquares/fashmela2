import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { DataService } from "../data.service";
import { Md5 } from "ts-md5/dist/md5";
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-add-business-profile',
  templateUrl: './add-business-profile.component.html',
  styleUrls: ['./add-business-profile.component.css']
})
export class AddBusinessProfileComponent implements OnInit {
  editBusinessForm: FormGroup;
  message: object;
  userId : any;
  profileDataEdit: object;
  submitted = false;
  profileData: any;
  firstName: any;
  base64textString: any;
  editFromimg: any;
  base64textStringForSave: any;
  imgFlag: boolean;
  noImgFlag: boolean;
  base64ImgFlag: boolean;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private FormBuilder: FormBuilder,
    private DataService: DataService,
    private route: ActivatedRoute,
    private Router: Router,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.userId = localStorage.getItem('userId');
    this.editBusinessForm = this.FormBuilder.group({
      userId: [''],
      businessProfileId: [''],
      companyName: ['', [Validators.required, Validators.pattern('^[^\s]+[-a-zA-Z0-9\s]+([-a-zA-Z0-9]+)*$')]],
      accountNumber: ['', [Validators.required, Validators.pattern('^[^\s]+[-a-zA-Z0-9\s]+([-a-zA-Z0-9]+)*$')]],
      holderName: ['', [Validators.required, Validators.pattern('^[^\s]+[-a-zA-Z0-9\s]+([-a-zA-Z0-9]+)*$')]],
      ifscCode: ['', [Validators.required, Validators.pattern('^[^\s]+[-a-zA-Z0-9\s]+([-a-zA-Z0-9]+)*$')]],

    });
  }
  get f() {
    return this.editBusinessForm.controls;
  }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.editBusinessForm.invalid) {
      return;
    }
    this.spinner.show();

    let editBusinessFormValue = this.editBusinessForm.value;
    var userJson = {}
    userJson = { "userId": this.userId, "isActive": false }
    //call login api here
    this.DataService.addBusinessDetails(this.editBusinessForm.value, userJson).subscribe(resultCategories => {
      this.spinner.hide();
      if (resultCategories.statusCode == 200) {
        this.Router.navigateByUrl('/business-profile');
      } else {
        this.message = resultCategories;
      }
    },
      error => {

      })

  }
  onKey(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
