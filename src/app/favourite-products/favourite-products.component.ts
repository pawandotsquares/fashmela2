import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { myfavouriteItems } from '../util/helper';

@Component({
  selector: 'app-favourite-products',
  templateUrl: './favourite-products.component.html',
  styleUrls: ['./favourite-products.component.css']
})
export class FavouriteProductsComponent implements OnInit {
  purchaseType: any;
  private status : boolean = true;
  products:any;
  userId:any;
  actiontype:any;
  actionvalue:any;
  p:any;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor( private dataservice: DataService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,) {
      this.route.params.subscribe(params => this.getProductByPurchaseType(params['id']));

  }
  getProductByPurchaseType(id: any) {
    if (id && id == 1 || id == 2) {
      localStorage.setItem('purchaseType', id);
    }
  }

  ngOnInit() {
    this.p=1;
    this.spinner.show();
    if (localStorage.getItem('purchaseType')){
      this.purchaseType = localStorage.getItem('purchaseType')
    }else{
      this.purchaseType = 1;
    }
    let userId = localStorage.getItem('userId');
    //alert(userId);
    this.favouriteProduct(this.purchaseType,userId);

  }

  myfavouriteItems(productId,actionvalue,idx){
    let userId = localStorage.getItem('userId');
    if(actionvalue){
      //this.products[idx]['favorite']= true
    }else{
      //this.products[idx]['favorite']= false
      this.favouriteProduct(this.purchaseType, userId);
    }

    myfavouriteItems(actionvalue, productId,userId, this.actiontype, this.spinner, this.dataservice, this.products);
  }


  favouriteProduct(purchaseType:any,userId) {
    this.spinner.show();
      this.status = !this.status;
      this.dataservice.getfavouriteProducts(purchaseType,userId).subscribe(resultFavouriteProducts => {
      this.spinner.hide();
      //console.log(resultFavouriteProducts);
      if(resultFavouriteProducts.statusCode==200){
        this.products = resultFavouriteProducts.responsePacket;
        console.log(this.products)
      }
    },
      error => {
        //(err) => console.log(err);
      })
  }

}
