import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  categoryData: object;
  message: object;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private DataService: DataService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.categoryList();
  }
    categoryList(){
      this.spinner.show();
      this.DataService.getCategories().subscribe(resultCategoryData=>{
        this.spinner.hide();
        if (resultCategoryData.statusCode==200){
          this.categoryData = resultCategoryData.responsePacket;
        }else{
          this.message = resultCategoryData.message;
        }
      }
      )
    }
}
