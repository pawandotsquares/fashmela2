import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  userId:any;
  chat:any;
  msg:any;
  tcode:string;
  Isimage:any;
  sendMessageObj:any;
  responseSendData:any;
  uploadImage:any;
  msgtext:string;
  Isimageflag:string;
  getsendMessageResponse:any;
  imgURL: any;
  imagePath:any;
  msgtextarea:any;

  base64textString: any;
  base64textStringForSave: any;
  base64ImgFlag: boolean;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private dataservice: DataService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService
    ) { }


  ngOnInit() {
    this.spinner.show();
    this.userId = localStorage.getItem('userId');
    //this.userId =7;

    //this.getChatHistory(this.userId);

    // setInterval(() => {
    //   this.getChatHistory(this.userId);
    // }, 800000);

  }

  getChatHistory(userId){
    //console.log('jai');
      this.dataservice.getChatHistory(userId,'getChatHistory').subscribe(resultChatHistory => {
      this.spinner.hide();
      if(resultChatHistory.statusCode==200){
        this.chat = resultChatHistory.responsePacket.chatMessageList;
      }
      //console.log(this.chat);
    },
      error => {
        //(err) => console.log(err);
    })
  }

  onFileChange(event) {
    var files = event.target.files;
    var file = files[0];

    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      //var emptyFile = document.createElement('input');
      //emptyFile.type = 'file';
      alert("Only images are supported.");

      //files.files = emptyFile.files;
      return false;
    }


    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      //this.imgURL = reader.result;
      // this.imagePath = files;
      // reader.readAsDataURL(file);
      // reader.onload = (_event) => {
      //   this.imgURL = reader.result;
      // }

      reader.readAsBinaryString(file);

    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
   // alert(binaryString);
    this.base64textString = 'data:image/png;base64,' + btoa(binaryString);
    this.base64textStringForSave = btoa(binaryString);
    this.submit();

  }


  submit(){
    //this.spinner.show();
    //console.log(this.base64textStringForSave);
    //var image_name = this.uploadImage.match( /[\w-]+\.(jpg|png|gif)/g );
    // var base64 = this.getBase64(image_name);
    //let Isimageflag = {};
    if(this.base64textStringForSave){
      this.Isimageflag='true';
      this.msgtext = this.base64textStringForSave;
    }else{
      this.Isimageflag='false';
      this.msgtext = this.msgtextarea;
    }
    //console.log(this.msgtext);
    if(!this.msgtext) {
      alert("Please enter message.");
      this.msgtextarea = '';
      return false;
    }
    //this.userId={};
    this.base64textStringForSave
    let sendMessageObj = {};
    this.sendMessageObj = {
      sender:{
        userId: this.userId
      },
      receiver:{
        userId: 1
      },
      message: this.msgtext,
      isImage :this.Isimageflag
    }

    sendMessageObj = JSON.stringify(this.sendMessageObj);
    //console.log(sendMessageObj);
    //return;
    this.dataservice.sendChatMessage(sendMessageObj,'sendMessage').subscribe(resultsendMessage => {
   // this.spinner.hide();

      this.responseSendData=resultsendMessage.responsePacket
      //console.log(this.responseSendData);
        if (resultsendMessage.statusCode == 200) {
         // this.spinner.hide();
          this.base64textStringForSave = "";
          this.getChatHistory(this.userId);
          this.msgtextarea = '';

          //document.getElementById("msgInput").value = "";
            // getIsImage = this.responseSendData.responsePacket.isImage;
            //this.getsendMessageResponse=this.responseSendData.responsePacket.message;
           //this.chat = resultChatHistory.responsePacket.chatMessageList;

        }

    },
      error => {
        //(err) => console.log(err);
    })

  }

}
