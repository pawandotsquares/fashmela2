import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { Md5 } from 'ts-md5/dist/md5';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { User } from 'src/app/models/user';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  message: any;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  public User: User = new User({});
  constructor(
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    private router: Router,
    private spinner: NgxSpinnerService

    ) { }

  ngOnInit() {

    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', Validators.required],
      rememberme: ['']
    });

    var loginFormObj = {};
    let remEmail = localStorage.getItem('remEmail');
    let remPass = localStorage.getItem('remPass');
    let getRememberMe = localStorage.getItem('rememberMe');
    if (getRememberMe) {

      loginFormObj = {
        email: remEmail,
        password: remPass,
        rememberme: true
      };

      this.loginForm.setValue(loginFormObj);
    }

  }

  // convenience getter for easy access to form fields
  get f() {

    return this.loginForm.controls;

  }

  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.spinner.show();
    let rememberMe = this.f.rememberme.value;

    if (this.f.email.value) {

      const md5 = new Md5();

      let email = this.f.email.value;
      let remPassword = this.f.password.value;

      let password = md5.appendStr(this.f.password.value).end();

      //call login api here
      this.dataservice.userlogin(email, password).subscribe(loginResult => {
        console.log(loginResult);
        this.spinner.hide();
        if (loginResult.statusCode == 200) {
          this.message = loginResult;

          this.User = new User(loginResult.responsePacket)
          this.dataservice.setCurrentUser(this.User);

          if (rememberMe){
            localStorage.setItem('rememberMe', rememberMe);
            localStorage.setItem('remEmail', email);
            localStorage.setItem('remPass', remPassword);
          }else{
            localStorage.setItem('rememberMe', '');
            localStorage.setItem('remEmail', '');
            localStorage.setItem('remPass', '');
          }

          localStorage.setItem('isLoggedIn', "true");
          localStorage.setItem('user_session_data', loginResult);
          localStorage.setItem('token', this.f.email.value);
          localStorage.setItem('authtoken', this.User.authToken);
          localStorage.setItem('userId', loginResult.responsePacket.userId);
          localStorage.setItem('email', loginResult.responsePacket.email);
          localStorage.setItem('admin_session_data', loginResult.responsePacket);
          this.setLoggedInTime();
          let pageUrl = localStorage.getItem('pageUrl');
          if (pageUrl){
            this.router.navigateByUrl(pageUrl);
            localStorage.setItem('pageUrl', "");
          }else{
            this.router.navigateByUrl('/profile');
          }
        } else {
          this.message = loginResult;
          localStorage.setItem('statusCode', loginResult.message);
        }

      },
        error => {

        })
    } else {
      alert('Credentials are incorrect.');
    }

  }
  setLoggedInTime() {
    var loginTime = new Date().getTime();
    localStorage.setItem("LoggedInTime", loginTime.toString())
  }
}
