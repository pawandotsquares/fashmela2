import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { Router, NavigationEnd } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Alert } from 'selenium-webdriver';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contactForm: FormGroup;
  submitted = false;
  message: any;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });


    this.contactForm = this.formBuilder.group({
  /*     name: ['', [Validators.required, Validators.pattern('^[^\s]+[-a-zA-Z0-9\s]+([-a-zA-Z0-9]+)*$')]],
      email: ['', [Validators.required, Validators.email]],
      subject: ['', [Validators.required, Validators.pattern('^[^\s]+[-a-zA-Z0-9\s]+([-a-zA-Z0-9]+)*$')]],
      message: ['', [Validators.required, Validators.pattern('^[^\s]+[-a-zA-Z0-9\s]+([-a-zA-Z0-9]+)*$')]], */

      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      subject: ['', Validators.required,],
      message: ['', Validators.required,],

    });

  }
  // convenience getter for easy access to form fields
  get f() {
    return this.contactForm.controls;
  }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.contactForm.invalid) {
      return;
    }
    this.spinner.show();
    //call login api here
    this.dataservice.sendContactQuery(this.contactForm.value).subscribe(result => {
      this.spinner.hide();
      if (result.statusCode == 200) {
        alert("Email sent successfully")
        this.message = result;
        window.location.reload();
        this.router.navigateByUrl('/contact');
      } else {
        this.message = result;
      }
    },
      error => {

      })
  }
}
