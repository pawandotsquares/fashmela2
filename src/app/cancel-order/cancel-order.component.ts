import { Component, OnInit } from '@angular/core';
import { DataService } from "../data.service";
import { Md5 } from "ts-md5/dist/md5";
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-cancel-order',
  templateUrl: './cancel-order.component.html',
  styleUrls: ['./cancel-order.component.css']
})
export class CancelOrderComponent implements OnInit {

  returnRequestForm: FormGroup;
  userId : any;
  message: any;
  reasonResponse:object;
  orderId:any;
  itemorderuuId:any;
  urls = [];
  submitted = false;
  reasonValue:object;
  imageBlob:string;
  

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private FormBuilder: FormBuilder,
    private DataService: DataService,
    private route: ActivatedRoute,
    private Router: Router,
    private spinner: NgxSpinnerService
  ) { }

  changeReason(reason){
    this.reasonValue=reason.value
  }
  ngOnInit() {
    this.userId = localStorage.getItem('userId');
    this.orderId = this.route.snapshot.queryParamMap.get('id');
    this.itemorderuuId =this.route.snapshot.queryParamMap.get('itemuuid');

    // this.route.params.subscribe(params => params['id']);
    // this.route.params.subscribe(params => {
    //   this.orderId = params['id'];
    // });
    //console.log(this.route.params);
    this.returnRequestForm = this.FormBuilder.group({
    userId: [''],
    reason:[''],
    tellUsMore: ['']
    });

    this.spinner.show();
	  this.DataService.returnOrder('getDefaultReturnReasonsList','').subscribe(resultReasonData => {
		this.spinner.hide();
	  if (resultReasonData.statusCode == 200) {
      this.reasonResponse = resultReasonData.responsePacket;
    }else{
		  this.message = resultReasonData.message;
		}
	 })
  }
  get f() {
    return this.returnRequestForm.controls;
  }
  
  onSubmit() {
    let orderuuid = this.orderId
    let itemuuid = this.itemorderuuId
    var retVal = confirm("Do you want to continue ?");
    if( retVal == true ) {
      this.submitted = true;
      let tellUsMore: string = this.f.tellUsMore.value;
      let reasonSelect: object = this.reasonValue;
      // stop here if form is invalid
      if (this.returnRequestForm.invalid) {
        return;
      }
      //this.spinner.show();
      //let submitFormValue = this.returnRequestForm.value;
      var returnJson = {}
      //orderStatus:7 for order return status
     // returnJson = { "userId": this.userId, "orderUuid": this.orderId ,"reasonId":reasonSelect,"image":"","orderStatus":7}

      if(itemuuid){
        var APIaction='returnSingleItem';
        returnJson = { "userId": this.userId, "orderUuid": this.orderId ,"reasonId":reasonSelect,"image":this.imageBlob,"orderStatus":7,"productOrderUUId":itemuuid}
      }else{
        var APIaction='returnOrder';
        returnJson = { "userId": this.userId, "orderUuid": this.orderId ,"reasonId":reasonSelect,"image":this.imageBlob,"orderStatus":7}
      }

      //call login api here
      this.DataService.returnOrder(APIaction,returnJson).subscribe(resultreturnOrder => {
        this.spinner.hide();
        
        if (resultreturnOrder.statusCode == 200) {
          alert("This order cancel successfully");
          if(itemuuid){
            this.Router.navigateByUrl('/order-details/'+orderuuid);
          }else{
            this.Router.navigateByUrl('/order-history/');
          }

         // this.Router.navigateByUrl('/order-details/'+orderuuid);
        } else {
          this.message = "something went wrong please try again";
        }
      },
        error => {
          this.message = "something went wrong please try again";
        })
    } else {
      this.Router.navigateByUrl('/order-details/'+orderuuid);
      return false;
    }


   

  }
}
