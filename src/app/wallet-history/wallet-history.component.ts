import { Component, OnInit } from '@angular/core';
import { DataService } from "../data.service";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-wallet-history',
  templateUrl: './wallet-history.component.html',
  styleUrls: ['./wallet-history.component.css']
})
export class WalletHistoryComponent implements OnInit {
  walletData: object;
  message: any;
  noImg: boolean;
  noMsg: boolean;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private dataservice: DataService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.noImg = true;;
    let userId = localStorage.getItem('userId');
    this.spinner.show();
    this.walletDetails(userId)
    this.noMsg = false;
  }
  walletDetails(userId) {

    this.dataservice.walletHistory(userId).subscribe(resultwalletData => {
      this.spinner.hide();
      this.noMsg = true;

      if (resultwalletData.statusCode == 200) {
        this.walletData = resultwalletData.responsePacket.historyList
      } else {
        this.message = resultwalletData;
      }
    },
      error => {

      })
  }
}
