import { Component, OnInit } from '@angular/core';
import { DataService } from "../data.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {
  orderData: object;
  message: any;
  orderId: any;
  noImg: boolean;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private dataservice: DataService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.orderId = params['id'];
    });
    this.noImg = true;;

    this.spinner.show();
    this.orderHistory(this.orderId)
  }

  orderHistory(orderId) {
    this.dataservice.orderDetails(orderId).subscribe(resultorderData => {
      this.spinner.hide();
      if (resultorderData.statusCode == 200) {
        console.log(resultorderData);
        if (resultorderData.responsePacket.walletImage) {
          this.noImg = false;
        }
        this.orderData = resultorderData.responsePacket
      } else {
        this.message = resultorderData;
        localStorage.setItem('statusCode', resultorderData.message);
      }
    },
      error => {

      })
  }

  perCalculate(price, itemDiscount) {
    var discount = (itemDiscount / price) * 100
    return discount;
  }


}
