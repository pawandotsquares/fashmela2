import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  productData: object;
  productListData: any;
  productListDataElse: any;
  message: object;
  purchaseType: any;
  searchParam: string;

  constructor(
    private dataservice: DataService,
    private route:ActivatedRoute,
    private router:Router,
    private spinner: NgxSpinnerService
  ) {
    this.route.params.subscribe(params => this.getProductListByID(params['id']));
  }
  getProductListByID(id: number) {
    if(!id){
      id = 0;
    }
    if (!isNaN(id)) {
      this.purchaseType = localStorage.getItem('purchaseType');
      this.dataservice.getParentProducts(id, this.purchaseType).subscribe(resultproductDetails => {

        this.spinner.hide();
        if (resultproductDetails.statusCode == 200) {
          this.productListData = resultproductDetails.responsePacket;
          this.productListDataElse = resultproductDetails.responsePacket.length;

        }else{
          this.message = resultproductDetails.message;

        }
      })
    }

  }
  ngOnInit() {
    this.spinner.show();
    this.searchParam = this.route.snapshot.queryParamMap.get("search")
    this.purchaseType = localStorage.getItem('purchaseType');
    if (this.searchParam){
      this.dataservice.getProductListBySearch(this.searchParam, this.purchaseType).subscribe(resultproductDetails => {

        this.spinner.hide();
        if (resultproductDetails.statusCode == 200) {
          this.productListData = resultproductDetails.responsePacket.productList;
          this.productListDataElse = resultproductDetails.responsePacket.length;

        }else{
          this.productListDataElse = true;
          this.message = resultproductDetails.message;

        }
      })
    }else{
      if (this.searchParam!=null) {
        this.router.navigateByUrl('/');
      }
    }
  }
}
