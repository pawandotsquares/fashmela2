import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-address',
  templateUrl: './add-address.component.html',
  styleUrls: ['./add-address.component.css']
})
export class AddAddressComponent implements OnInit {

  addressForm: FormGroup;
  submitted = false;
  message: any;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {

    this.addressForm = this.formBuilder.group({
      name: ['', Validators.required],
      mobile_number: ['', [Validators.required, Validators.min(1000000000), Validators.max(9999999999)]],
      building: ['',Validators.required],
      locality: ['',Validators.required],
      pin: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      fromName: [''],
      fromContactNumber: ['', [Validators.min(1000000000), Validators.max(9999999999)]]
      // fromAddress: ['']
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.addressForm.controls;
  }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.addressForm.invalid) {
      return;
    }
    const userId = localStorage.getItem('userId');
    this.spinner.show();
    //call login api here
    this.dataservice.adduserAddress(this.addressForm.value, userId).subscribe(result => {
      this.spinner.hide();
      if (result.statusCode == 200) {
        this.message = result;
        this.router.navigateByUrl("/view-address");

      } else {
        this.message = result;
      }
    },
      error => {

      })
  }
  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
