import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { Md5 } from 'ts-md5/dist/md5';
import { Router } from '@angular/router';
import { AccountKit, AuthResponse } from 'ng2-account-kit';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
	selector: 'app-vendor',
	templateUrl: './vendor.component.html',
	styleUrls: ['./vendor.component.css']
})
export class VendorComponent implements OnInit {

	registerForm: FormGroup;
	submitted = false;
	message: any;
	confirmPassNotValid: boolean;

	public spinnerConfig: any = {
		bdColor: 'rgba(51,51,51,0.8)',
		size: 'large',
		color: '#fff',
		type: 'ball-circus',
		loadigText: 'Loading...'
	};


	constructor(private formBuilder: FormBuilder, private dataservice: DataService, private router: Router, private spinner: NgxSpinnerService) { }

	ngOnInit() {
		if (localStorage.getItem('isLoggedIn') == "true") {
			//this.router.navigate(['/profile']);
		}
		AccountKit.init({
			appId: "2141198699233404",
			state: "1234",
			version: "v1.1",
		});


		this.registerForm = this.formBuilder.group({
			firstName: ['', Validators.required],
			email: ['', [Validators.required, Validators.email]],
			phoneNumber: ['', [Validators.required, Validators.min(1000000000), Validators.max(9999999999)]],
			companyName: ['', Validators.required],
			panCardNumber: ['', Validators.required],
			gstNumber: ['', Validators.required],
			companyUrl: ['']
		},
		);

	}
	checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {

		return (group: FormGroup) => {
			let passwordInput = group.controls[passwordKey],
				passwordConfirmationInput = group.controls[passwordConfirmationKey];
			if (passwordConfirmationInput.value) {
				if (passwordInput.value !== passwordConfirmationInput.value) {
					return passwordConfirmationInput.setErrors({ notEquivalent: true })
				} else {
					return passwordConfirmationInput.setErrors(null);
				}
			}
		}
	}
	// convenience getter for easy access to form fields
	get f() {
		return this.registerForm.controls;
	}

	onSubmit() {

		this.submitted = true;

		// stop here if form is invalid
		if (this.registerForm.invalid) {
			return;
		}
		this.spinner.show();
		console.log(this.registerForm.value)

		//call login api here
		this.dataservice.vendorRegister(this.registerForm.value).subscribe(result => {
			this.spinner.hide();
			if (result.statusCode == 200) {
				this.message = result;
				window.location.reload();
				alert("Your account has been created, You will receive an email for further process.");
			} else {
				this.message = result;
			}
		},
			error => {

			})
		}
	keyPress(event: any) {
		const pattern = /[0-9\+\-\ ]/;

		let inputChar = String.fromCharCode(event.charCode);
		if (event.keyCode != 8 && !pattern.test(inputChar)) {
			event.preventDefault();
		}
	}

}
