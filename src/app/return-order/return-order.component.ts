import { Component, OnInit } from '@angular/core';
import { DataService } from "../data.service";
import { Md5 } from "ts-md5/dist/md5";
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-return-order',
  templateUrl: './return-order.component.html',
  styleUrls: ['./return-order.component.css']
})
export class ReturnOrderComponent implements OnInit {
  returnRequestForm: FormGroup;
  userId : any;
  message: any;
  reasonResponse:object;
  orderId:any;
  itemorderuuId:any;
  urls = [];
  submitted = false;
  reasonValue:object;
  imageBlob:string;
  

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private FormBuilder: FormBuilder,
    private DataService: DataService,
    private route: ActivatedRoute,
    private Router: Router,
    private spinner: NgxSpinnerService
  ) { }


  changeShape(reason){
    this.reasonValue=reason.value
  }
  ngOnInit() {
    this.userId = localStorage.getItem('userId');
    this.orderId = this.route.snapshot.queryParamMap.get('id');
    this.itemorderuuId =this.route.snapshot.queryParamMap.get('itemuuid');
    //this.orderId = this.route.snapshot.queryParamMap.get('id');
    //this.route.params.subscribe(params => params['id']);
    //console.log(this.route.snapshot.queryParamMap.get('id'));
    // this.route.params.subscribe(params => {
      
    //   this.orderId = params['id'];

    //   console.log(this.orderId);
    //   this.itemorderuuId = params['itemuuid'];
    // });

    
    this.returnRequestForm = this.FormBuilder.group({
    userId: [''],
    reason:[''],
    tellUsMore: ['']
  
      //reason: ['', [Validators.required, Validators.pattern('^[^\s]+[-a-zA-Z0-9\s]+([-a-zA-Z0-9]+)*$')]],
     // tellUsMore: ['', [Validators.required, Validators.pattern('^[^\s]+[-a-zA-Z0-9\s]+([-a-zA-Z0-9]+)*$')]],
     
    });

    this.spinner.show();
	  this.DataService.returnOrder('getDefaultReturnReasonsList','').subscribe(resultReasonData => {
		this.spinner.hide();
	  if (resultReasonData.statusCode == 200) {
      this.reasonResponse = resultReasonData.responsePacket;
      //
	  }else{
		  this.message = resultReasonData.message;
		
	  }
	 })
  }
  get f() {
    //console.log(this.returnRequestForm.controls);
    return this.returnRequestForm.controls;
  }
  
  onSelectFile(event) {
    this.urls = [];
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.urls.push(e.target.result);
        }
        reader.readAsDataURL(file);
      }
    }
    //Show multiple images
    // if (event.target.files && event.target.files[0]) {
    //     var filesAmount = event.target.files.length;
    //     for (let i = 0; i < filesAmount; i++) {
    //             var reader = new FileReader();
    //             reader.onload = (event) => {
    //               this.imageBlob=event.target.result;
    //              // console.log(event.target.result);
    //                this.urls.push(event.target.result); 
    //             }
    //             reader.readAsDataURL(event.target.files[i]);
    //     }
    // }
  }


  onSubmit() {
   let orderuuid = this.orderId
   let itemuuid = this.itemorderuuId
    var retVal = confirm("Do you want to return this item ?");
      if( retVal == true ) {
      this.submitted = true;
      let tellUsMore: string = this.f.tellUsMore.value;
      let reasonSelect: object = this.reasonValue;
      // stop here if form is invalid
      if (this.returnRequestForm.invalid) {
        //console.log('blank');
        return;
      }
      var returnJson = {}
      //orderStatus:7 for order return status
      if(itemuuid){
        var APIaction='returnSingleItem';
        returnJson = { "userId": this.userId, "orderUuid": this.orderId ,"reasonId":reasonSelect,"image":this.imageBlob,"orderStatus":7,"productOrderUUId":itemuuid}
      }else{
        var APIaction='returnOrder';
        returnJson = { "userId": this.userId, "orderUuid": this.orderId ,"reasonId":reasonSelect,"image":this.imageBlob,"orderStatus":7}
      }
      
      //call login api here
      this.DataService.returnOrder(APIaction,returnJson).subscribe(resultreturnOrder => {
      this.spinner.hide();
      // console.log(resultreturnOrder);
      if (resultreturnOrder.statusCode == 200) {
          alert("This Order return sccessfully");
          if(itemuuid){
            this.Router.navigateByUrl('/order-details/'+orderuuid);
          }else{
            this.Router.navigateByUrl('/order-history/');
          }
          
        } else {
          this.message = "something went wrong please try again";
        }
      },
        error => {
          this.message = "something went wrong please try again";
        })
      } else {
        this.Router.navigateByUrl('/order-details/'+orderuuid);
        return false;
      }
  }

  OrderCancel(){
    // alert('hello');
     var retVal = confirm("Do you want to cancel the order ?");
     if( retVal == true ) {
        document.write ("User wants to continue!");
 
        return true;
     } else {
        document.write ("User does not want to continue!");
        return false;
     }
 
   }



}
