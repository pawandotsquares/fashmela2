import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AccountKit, AuthResponse } from 'ng2-account-kit';
import * as crypto from 'crypto-js';
//import { ConsoleReporter } from 'jasmine';


@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  forgotPasswordForm: FormGroup;
  submitted = false;
  message: any;
  email: any;


  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private formBuilder : FormBuilder,
    private dataservice: DataService,
    private router: Router,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    const secret = "@AKIAJSN26KWXJ45CJJRFUA@";
    this.forgotPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });

  }

  // convenience getter for easy access to form fields
  get f() {

    return this.forgotPasswordForm.controls;

  }

  onSubmit() {

    this.submitted = true;
    let email = this.f.email.value;

    this.spinner.show();

    if(Number(email))
    {
      this.spinner.hide();

      //call login api here
      this.dataservice.checkMobile('true','false',email).subscribe(result => {
        this.spinner.hide();
        console.log(result);
        if (result.statusCode == 200) {
          this.message = result;
          localStorage.setItem('statusCode', result.message);
           AccountKit.login('PHONE', { countryCode: "+91", phoneNumber: email }).then(
        (response: AuthResponse) => {
               console.log(response);
          if(response.state == '1234'){
            var encryptNumber = this.encrypt(email)

            this.router.navigateByUrl('/reset-password/' + encryptNumber);
          }

        },
        (error: any) => console.error(error)
      );


        } else {
          this.message = result;

          localStorage.setItem('statusCode', result.message);
        }

      },
        error => {

        })

     }else{
      this.spinner.show();

      if (this.f.email.value) {
        if (this.forgotPasswordForm.invalid) {
             return;
          }
        let email = this.f.email.value;

        //call login api here
        this.dataservice.forgotPass(email).subscribe(result => {
          this.spinner.hide();

          if (result.statusCode == 200) {
            this.message = result;
            localStorage.setItem('statusCode', result.message);
          } else {
            this.message = result;
            localStorage.setItem('statusCode', result.message);
          }

        },
          error => {

          })
      } else {
        alert('Invalid Email id.');
      }
     }
     return;
  }

  encrypt(data) {
    const secret = "@AKIAJSN26KWXJ45CJJRFUA@";
    return crypto.AES.encrypt(JSON.stringify(data), secret,
      {
        keySize: 128 / 8,
        iv: secret,
        mode: crypto.mode.CBC,
        padding: crypto.pad.Pkcs7
      }).toString();
  }
}
