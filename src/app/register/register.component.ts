import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { Md5 } from 'ts-md5/dist/md5';
import { Router } from '@angular/router';
import { AccountKit, AuthResponse } from 'ng2-account-kit';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  message: any;
  confirmPassNotValid:boolean;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };


  constructor(private formBuilder: FormBuilder, private dataservice: DataService, private router: Router, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    if (localStorage.getItem('isLoggedIn') == "true") {
      this.router.navigate(['/profile']);
    }
    AccountKit.init({
      appId: "2141198699233404",
      state: "1234",
      version: "v1.1",
    });


    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      mobile_number: ['', [Validators.required, Validators.min(1000000000), Validators.max(9999999999)]],
      userType: [''],
      userReferCode: [''],
      address: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirm_password: ['', Validators.required]
    }, { validator: this.checkIfMatchingPasswords('password', 'confirm_password') });

  }
  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {

    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordConfirmationInput.value){
          if (passwordInput.value !== passwordConfirmationInput.value) {
            return passwordConfirmationInput.setErrors({ notEquivalent: true })
          } else {
            return passwordConfirmationInput.setErrors(null);
          }
        }
    }
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  /* login(): void {

  }
 */
  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.spinner.show();
    let email = this.f.email.value;

    const md5 = new Md5();
    let password = md5.appendStr(this.f.password.value).end();

    let mobile_number: string = this.f.mobile_number.value;
    //call login api here
    this.dataservice.register(this.registerForm.value, password).subscribe(result => {
      this.spinner.hide();
      if (result.statusCode == 200) {

        this.message = result;
        localStorage.setItem('isLoggedIn', "true");
        localStorage.setItem('user_session_data', result);
        localStorage.setItem('token', this.f.email.value);
        localStorage.setItem('userId', result.responsePacket.userId);
        localStorage.setItem('email', email);

        this.router.navigateByUrl('/profile');
      } else {
        this.message = result;
      }
    },
      error => {

      })
     /* AccountKit.login('PHONE', { countryCode: "+91", phoneNumber: mobile_number }).then(
      (response: AuthResponse) => {
        if(response.state == '1234'){
          const md5 = new Md5();
          let registerFormValue = this.registerForm.value;
          registerFormValue.password = md5.appendStr(registerFormValue.password).end();

          //call login api here
          this.dataservice.register(this.registerForm.value, registerFormValue.password).subscribe(result => {

            if (result.statusCode == 200){

              this.message = result;
              localStorage.setItem('isLoggedIn', "true");
              localStorage.setItem('user_session_data', result);
              localStorage.setItem('token', this.f.email.value);
              localStorage.setItem('userId', result.responsePacket.userId);
              localStorage.setItem('email', email);

              this.router.navigateByUrl('/profile');
            }else{
              this.message = result;
            }
          },
            error => {

            })
        }

      },
      (error: any) => console.error(error)
    );
 */
  }
  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}
