import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-wholesale',
  templateUrl: './wholesale.component.html',
  styleUrls: ['./wholesale.component.css']
})
export class WholesaleComponent implements OnInit {
  purchaseType: any;
  userId: any;
  products: object;
  categoryList: object;
  trendingproduct: object;
  slides: object;
  slideConfig: object;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private dataservice: DataService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) {
    this.route.params.subscribe(params => this.getProductByPurchaseType(params['id']));
  }
  getProductByPurchaseType(id: any) {
    if (id && id == 1 || id == 2) {

      localStorage.setItem('purchaseType', id);
    }
  }
  ngOnInit() {
    this.spinner.show();
    this.userId = localStorage.getItem('userId');
    if (localStorage.getItem('purchaseType')) {
      this.purchaseType = localStorage.getItem('purchaseType')
    } else {
      this.purchaseType = 1;
    }
    this.categories()
    this.featuredProduct(this.purchaseType)
    this.trendingProducts(this.purchaseType)

    this.slideConfig = {
      "slidesToShow": 3,
      "slidesToScroll": 3,
      "arrows": true,
      "prevArrow": '<button type="button" class="slick-prev"><img src="assets/images/arow-left.png"></button>',
      "nextArrow": '<button type="button" class="slick-next"><img src="assets/images/arow-right.png"></button>',
    };
  }
  featuredProduct(purchaseType: any) {
    this.dataservice.getFeaturedProducts(purchaseType,this.userId).subscribe(resultFeaturedProduct => {
      this.spinner.hide();
      this.products = resultFeaturedProduct.responsePacket
    },
      error => {

      })
  }

  categories() {
    this.dataservice.getCategories().subscribe(resultCategories => {
      this.spinner.hide();
      this.categoryList = resultCategories.responsePacket
    },
      error => {

      })
  }

  trendingProducts(purchaseType: any) {
    this.dataservice.getTrendingProducts(purchaseType, this.userId).subscribe(resultTrendingProduct => {
      this.spinner.hide();
      this.trendingproduct = resultTrendingProduct.responsePacket
    },
      error => {

      })
  }

}
