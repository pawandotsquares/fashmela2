import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { myfavouriteItems } from '../util/helper';

@Component({
  selector: 'app-sub-product',
  templateUrl: './sub-product.component.html',
  styleUrls: ['./sub-product.component.css']
})
export class SubProductComponent implements OnInit {

  message: any;
  noMsg: boolean;
  parentProductId:any;
  purchaseType:any;
  getsubproducts:object;
  products: object;
  userId:any;
  actiontype:any;
  subproducts:object;
  limitTo:any;
  p: number = 1;
  constructor(
    private dataservice: DataService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private Router: Router,
  ) { }

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  ngOnInit() {
    this.limitTo=0;
    this.spinner.show();
    this.userId = localStorage.getItem('userId');
    this.route.params.subscribe(params => {
      this.parentProductId = params['id'];
    });
    this.purchaseType = localStorage.getItem('purchaseType');
    this.showSubProducts(this.parentProductId,this.purchaseType)
    //this.featuredProduct(this.purchaseType);
  }

  showSubProducts(parentProductId,purchaseType) {
    this.dataservice.getSubProducts(parentProductId,purchaseType,this.userId).subscribe(resultsubproducts => {

      this.spinner.hide();
      this.noMsg = true;
			if(resultsubproducts.statusCode == 200) {
        console.log(resultsubproducts);
        this.getsubproducts = resultsubproducts.responsePacket.subProducts;
        }else {
          this.message = resultsubproducts;
        }
      },
      error => {

      })
  }

  myfavouriteProducts(productId,actionvalue,idx){
    if (localStorage.getItem('userId')) {
      let userId = localStorage.getItem('userId');
    } else {
      this.Router.navigateByUrl('/login');
    }
    if(actionvalue){
      this.getsubproducts[idx]['favorite']= true
    }else{
      this.getsubproducts[idx]['favorite']= false
      //this.featuredProduct(this.purchaseType);
    }
    myfavouriteItems(actionvalue, productId, this.userId, this.actiontype, this.spinner, this.dataservice, this.products);
  }

}
