import { Component, OnInit } from '@angular/core';
import { DataService } from "../data.service";
import { NgxSpinnerService } from 'ngx-spinner';



@Component({
  selector: 'app-earnbonus',
  templateUrl: './earnbonus.component.html',
  styleUrls: ['./earnbonus.component.css']
})
export class EarnbonusComponent implements OnInit {

  message: any;
  resultBonusData: any;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };
  constructor(
    private dataservice: DataService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    let userId = localStorage.getItem('userId');
    this.spinner.show();
    this.getEarnBonusData(userId)
  }

  getEarnBonusData(userId) {
    this.dataservice.getEarnBonusData(userId, 'getEarnBonusData').subscribe(resultBonusData => {
      this.spinner.hide();
      if (resultBonusData.statusCode == 200) {
        console.log(resultBonusData['responsePacket'])
        this.resultBonusData = resultBonusData['responsePacket']

      } else {
        this.message = resultBonusData.message;
      }
    },
      error => {

      })
  }

}
