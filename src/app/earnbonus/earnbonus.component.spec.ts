import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EarnbonusComponent } from './earnbonus.component';

describe('EarnbonusComponent', () => {
  let component: EarnbonusComponent;
  let fixture: ComponentFixture<EarnbonusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EarnbonusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarnbonusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
