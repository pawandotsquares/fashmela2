import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { NgxSpinnerService } from 'ngx-spinner';
import { SocialService } from "ng6-social-button";

@Component({
	selector: 'app-productdetails',
	templateUrl: './productdetails.component.html',
	styleUrls: ['./productdetails.component.css']
})

export class ProductdetailsComponent implements OnInit {
	products: object;
	slides: object;
	availableSizes: object;
	availableColors: any;
	slideConfig: object;
	productdata: any;
	productImages: any;
	description: any;
	productImagesOne: any;
	productDescription: any;
	similarProducts: any;
	images: any;
	arrJson = [];
	arrJsonColorSet = [];
	myJsonString: string;
	purchaseType: any;
	wholesale: boolean;
	single: boolean;
	productValue: any;
	productMRP: any;
	quantity: any;
	maxQuantity: any;
	zeroQuantity: boolean;
	studentDetails: object;
	FBVars: object;
	productId: any;
	sizeId: any;
	sizeListArr: any;
	finalShareValue: number;
	userId: any;
	colorset: any = 1;
	itemPrice: any;
	price: any;
	htmlOptions: any = { visible: 0 };
	entries = [];
	subProducts = [];
	subProductIds = [];
	shareProductImages = [];
	selectedColorSetSize: any;
	selectedColorSetSizeSelected: any;
	SubProIds: any;
	colorSetsizeId: object;
	discountType: any;
	itemDiscount: any;
	discount: any;

	ShareObj = {
		fbAppId: "",
		hashtag: "#FACEBOOK-SHARE-HASGTAG"
	};

	public spinnerConfig: any = {
		bdColor: 'rgba(51,51,51,0.8)',
		size: 'large',
		color: '#fff',
		type: 'ball-circus',
		loadigText: 'Loading...'
	};

	galleryOptions: NgxGalleryOptions[];
	galleryImages: NgxGalleryImage[];

	constructor(
		private dataservice: DataService,
		private route: ActivatedRoute,
		private spinner: NgxSpinnerService,
		private router: Router,
		private socialAuthService: SocialService
	) {
		this.route.params.subscribe(params => this.getProductDetailsByID(params['id']));
	}
	getProductDetailsByID(id: number) {
		this.productId = id;
		this.purchaseType = localStorage.getItem('purchaseType');
		if (this.purchaseType == 2) {
			this.wholesale = true;
			this.purchaseType = 2;
		} else {
			this.single = true;
			this.purchaseType = 1;
		}

		this.dataservice.getProductDetails(id, this.purchaseType).subscribe(resultproductDetails => {
			this.spinner.hide();

			if (resultproductDetails.statusCode == 200) {
				this.productdata = resultproductDetails.responsePacket;
				console.log(this.productdata);
				this.discountType = this.productdata.sizeList[0]['discountType'];
				this.itemDiscount = this.productdata.sizeList[0]['itemDiscount'];
				this.description = resultproductDetails.responsePacket.description;
				this.productImages = resultproductDetails.responsePacket.images;
				this.productDescription = resultproductDetails.responsePacket.description;

				this.similarProducts = resultproductDetails.responsePacket.subProducts;

				if (this.colorset == 2) {
					this.maxQuantity = resultproductDetails.responsePacket.quantity;
				}

				this.productImagesOne = resultproductDetails.responsePacket.displayImage;
				this.sizeListArr = resultproductDetails.responsePacket.sizeList;

				if (this.purchaseType == 1) {
					this.maxQuantity = resultproductDetails.responsePacket.sizeList[0].quantity;
					this.productValue = resultproductDetails.responsePacket.sizeList[0].totalPrice;
					this.productMRP = resultproductDetails.responsePacket.sizeList[0].mrp;
					this.sizeId = resultproductDetails.responsePacket.sizeList[0].sizeId;
					if (this.maxQuantity > 0) {
						this.zeroQuantity = true;
					} else {
						this.zeroQuantity = false;
					}


				} else {

					this.maxQuantity = resultproductDetails.responsePacket.quantity;
					this.productValue = resultproductDetails.responsePacket.totalPrice;
					this.productMRP = resultproductDetails.responsePacket.totalPrice;

					this.discountType = resultproductDetails.responsePacket.discountType;
					this.itemDiscount = resultproductDetails.responsePacket.itemDiscount;


					if (this.maxQuantity > 0) {
						this.zeroQuantity = true;
					} else {
						this.zeroQuantity = false;
					}

				}

				for (let image of this.productImages) {
					this.arrJson.push({ small: image, big: image, medium: image })
				}
				resultproductDetails.responsePacket.images.forEach(function (value) {

				});
				for (let image of this.productImages) {
					this.shareProductImages.push(image)
				}
			}

		}

		)
	}
	perCalculate(price, itemDiscount) {
		var discount = (itemDiscount / price) * 100
		return discount;
	}
	ngOnInit() {
		this.router.events.subscribe((evt) => {
			if (!(evt instanceof NavigationEnd)) {
				return;
			}
			window.scrollTo(0, 0)
		});
		this.quantity = 1;
		this.spinner.show();
		this.purchaseType = localStorage.getItem('purchaseType');

		this.userId = localStorage.getItem('userId');
		if (this.purchaseType == 2) {
			this.wholesale = true;
			this.purchaseType = 2;
		} else {
			this.single = true;
			this.purchaseType = 1;
		}

		if (this.maxQuantity > 0) {
			this.zeroQuantity = true;
		} else {
			this.zeroQuantity = false;
		}
		this.featuredProduct(this.purchaseType, this.userId);

		this.galleryOptions = [
			{
				width: '100%',
				height: '700px',
				thumbnailsColumns: 4,
				imageArrows: false,

			},
			{
				breakpoint: 800,
				width: '100%',
				height: '600px',
				imagePercent: 80,
				thumbnailsPercent: 20,
				thumbnailsMargin: 20,
				thumbnailMargin: 20
			},

			{
				breakpoint: 400,
				preview: true
			}
		];

		this.galleryImages = this.arrJson;
		//console.log(this.galleryImages);

		this.slideConfig = {
			"slidesToShow": 3,
			"slidesToScroll": 3,
			"arrows": true,
			"prevArrow": '<button type="button" class="slick-prev"><img src="assets/images/arow-left.png"></button>',
			"nextArrow": '<button type="button" class="slick-next"><img src="assets/images/arow-right.png"></button>',
		};

	}
	onClick(productValue, mQuantity, sizeId, productMRP, discountType, itemDiscount) {

		this.productValue = productValue;
		this.productMRP = productMRP;
		this.maxQuantity = mQuantity

		this.discountType = discountType
		this.itemDiscount = itemDiscount

		if (this.maxQuantity > 0) {
			this.zeroQuantity = true;
		} else {
			this.zeroQuantity = false;
		}
		this.sizeId = sizeId
		this.quantity = 1
	}
	qtyDecr(quantity) {
		if (quantity > 1) {
			this.quantity = quantity - 1;
		}
	}

	qtyIncr(quantity) {
		//console.log(this.maxQuantity);
		if (quantity < this.maxQuantity) {
			this.quantity = quantity + 1;
		} else {
			alert("You have exceeded the max quantity for this item");
		}
	}


	addToCart() {
		this.SubProIds = [];
		localStorage.setItem('pageUrl', this.router.url);
		if (localStorage.getItem('isLoggedIn') != "true") {
			this.router.navigate(['/login']);
			return false;
		}

		this.spinner.show();
		/* alert(this.productValue +" and "+ this.quantity); */


		let childProductIds = this.productdata.subProducts;
		//console.log(this.productdata);
		for (let proid of childProductIds) {
			this.subProductIds.push({ productId: proid.productId })
		}

		let SubProIds = this.subProductIds;
		if (this.purchaseType == "2") {

			if (this.colorset == 2) {
				this.sizeListArr = [
					{
						sizeId: this.selectedColorSetSizeSelected
					}
				]
			} else {
				this.sizeListArr = this.sizeListArr
			}

			//console.log(this.sizeListArr);

			for (let SingleChildid of SubProIds) {
				//console.log(proid.productId);
				this.subProducts.push({
					productId: SingleChildid.productId,
					purchaseType: 3,
					sizeList: this.sizeListArr
				})
			}

			//console.log(this.subProducts);

			//  this.subProducts = [

			// 	{
			// 		productId: this.productId,
			// 		purchaseType:3,
			// 		sizeList: this.sizeListArr

			// 	}

			// ]


		} else {
			this.sizeListArr = [
				{
					sizeId: this.sizeId
				}
			]
		}

		// if(this.colorset == 2){
		// 	this.purchaseType = 3;
		// }
		//alert(this.productId);
		this.sizeListArr
		let addToCartObj = {};

		if (this.colorset == 2) {
			addToCartObj = {
				user: {
					userId: localStorage.getItem('userId')
				},
				parentProduct: {
					productId: this.productdata.parentProductId,
					subProducts: this.subProducts
				},
				quantity: this.quantity
			};
		} else {
			addToCartObj = {
				user: {
					userId: localStorage.getItem('userId')
				},

				product: {
					productId: this.productId,
					purchaseType: this.purchaseType,
					sizeList: this.sizeListArr
				},
				quantity: this.quantity
			};
		}


		addToCartObj = JSON.stringify(addToCartObj)
		//console.log(addToCartObj);
		this.dataservice.addToCart(addToCartObj).subscribe(result => {

			if (result.statusCode == 200) {
				this.spinner.hide();
				this.router.navigateByUrl('', { skipLocationChange: true }).then(() =>
					this.router.navigate(["productdetails/" + this.productId]));
				alert("Added to cart")
			} else {
				alert(result.message)
			}
			this.spinner.hide();
		})
	}

	colorSetImages(val) {
		//alert(val);
		this.spinner.show();
		this.galleryImages = [];
		this.arrJsonColorSet = []
		if (this.availableColors) {
			this.spinner.hide();
			let image = this.availableColors;
			let imagearray = image[val]['images'];
			//this.itemPrice = image[val]['totalPrice'];
			//console.log(image[val]['totalPrice']);
			for (let image of imagearray) {
				// 	console.log(image);
				this.arrJsonColorSet.push({ small: image, big: image, medium: image })
			}
			// alert(image[val]['productId']);
			//	this.getProductDetailsByID(image[val]['productId']);
			this.galleryImages = this.arrJsonColorSet;
		}
	}

	availableSizeColor(parentProductId, colorSetsizeId) {
		this.quantity = 1;
		this.selectedColorSetSize = colorSetsizeId;
		this.selectedColorSetSizeSelected = colorSetsizeId
		//console.log(colorSetsizeId);
		// if(this.colorset==2){
		// 	alert('hello');
		// }

		this.spinner.show();
		//alert(parentProductId);
		this.dataservice.getProductDetailswithSize(3, parentProductId, this.selectedColorSetSize).subscribe(resultColorSizeProduct => {
			this.spinner.hide();
			if (resultColorSizeProduct.statusCode == 200) {
				//console.log(resultColorSizeProduct.responsePacket);
				this.availableSizes = resultColorSizeProduct.responsePacket.colorSetAvailableSizes
				this.availableColors = resultColorSizeProduct.responsePacket.subProducts
				this.itemPrice = resultColorSizeProduct.responsePacket.totalPrice;
				this.selectedColorSetSize = this.availableSizes[0].sizeId;
				this.maxQuantity = resultColorSizeProduct.responsePacket.quantity;
				this.itemDiscount = resultColorSizeProduct.responsePacket.itemDiscount;

				if (this.maxQuantity > 0) {
					this.zeroQuantity = true;
				} else {
					this.zeroQuantity = false;
				}
			}
		},
			error => {

			})

	}
	featuredProduct(purchaseType: any, userId) {
		this.dataservice.getFeaturedProducts(purchaseType, userId).subscribe(resultFeaturedProduct => {
			this.spinner.hide();
			if (resultFeaturedProduct.statusCode == 200) {
				this.products = resultFeaturedProduct.responsePacket
			}
		},
			error => {

			})
	}
	onKey(event: any, value: any) {

		let finalShareValue = (event.target.value / 100) * value;
		this.finalShareValue = value + finalShareValue;

	}
	createFBShareLink() {
		//console.log('hello'+this.shareProductImages);
		let FBVars = {
			fbAppId: "2141198699233404",
			fbShareImg: this.productImagesOne,
			fbShareName: "ghgf",
			fbShareCaption: "fghgfh",
			fbShareDesc: "ghfgh",
			baseURL: "https://d3rc17ccvsmeqb.cloudfront.net/productdetails/27"
		};
		var url = 'http://www.facebook.com/dialog/feed?app_id=' + FBVars.fbAppId +
			'&picture=' + FBVars.fbShareImg +
			'&name=' + encodeURIComponent(FBVars.fbShareName) +
			'&caption=' + encodeURIComponent(FBVars.fbShareCaption) +
			'&description=' + encodeURIComponent(FBVars.fbShareDesc) +
			'&display=popup';
		//console.log(url);

		window.open(url,
			'feedDialog',
			'toolbar=0,status=0,width=626,height=436'
		);
	}
	htmlToPlaintext(text) {
		return text ? String(text).replace(/<[^>]+>/gm, '') : '';
	}
	shareOnPinterest() {
		/* this.productDescription = "dfsd"; */
		var str = "http://pinterest.com/pin/create/button/?url=" + encodeURIComponent(window.location.href) + "&media=" + this.productImagesOne + "&description=" + this.htmlToPlaintext(this.productDescription);

		window.open(str,
			'_blank');

	}
	/* whatsApp() {
		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
			var article = "tests";
			var weburl = "fgdgf";
			var whats_app_message = encodeURIComponent(document.URL);
			var whatsapp_url = "whatsapp://send?text=" + whats_app_message;
			window.location.href = whatsapp_url;
		} else {
			alert('You Are Not On A Mobile Device. Please Use This Button To Share On Mobile');
		}
	} */
	numberOnly(event: any) {
		const charCode = (event.which) ? event.which : event.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}

	detectmob() {
		var isMobile = {
			Android: function () {
				return navigator.userAgent.match(/Android/i);
			},
			BlackBerry: function () {
				return navigator.userAgent.match(/BlackBerry/i);
			},
			iOS: function () {
				return navigator.userAgent.match(/iPhone|iPad|iPod/i);
			},
			Opera: function () {
				return navigator.userAgent.match(/Opera Mini/i);
			},
			Windows: function () {
				return navigator.userAgent.match(/IEMobile/i);
			},
			any: function () {
				return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
			}
		};

		if (isMobile.any()) {
			var text = this.description;
			var url = "https://d3rc17ccvsmeqb.cloudfront.net/productdetails/27"
			var image_url = this.productImagesOne
			var message = encodeURIComponent(text) + " - " + encodeURIComponent(url);
			var whatsapp_url = "whatsapp://send?text=" + message;
			window.location.href = whatsapp_url;
		} else {
			alert("This option is only available on mobile devices");
		}
	}
}
