import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { FacebookService, LoginResponse, LoginOptions, UIResponse, UIParams, FBVideoComponent } from 'ngx-facebook';

@Component({
  selector: 'app-referal-code',
  templateUrl: './referal-code.component.html',
  styleUrls: ['./referal-code.component.css']
})
export class ReferalCodeComponent implements OnInit {
  userId:any;
  //private dataservice:DataService;
  constructor(private fb: FacebookService,private dataservice:DataService) { 
    
    //console.log('Initializing Facebook');
    fb.init({
      appId: '2141198699233404',
      version: 'v2.9'
    });
    
  }

  ngOnInit() {
    //this.facebookshare();
    this.userId = localStorage.getItem('userId');
    this.userProfile(this.userId);
    
  }

  userProfile(userId) {
		this.dataservice.getUserProfile(userId).subscribe(resultuserProfile => {
      //console.log(resultuserProfile);
			if (resultuserProfile.statusCode == 200) {
        this.userProfile = resultuserProfile.responsePacket.userReferCode
       
			}
    },
      error => {

      })
  }


   /**
   * Show the share dialog
   */
  facebookshare() {
    //alert('facebook');
    const options: UIParams = {
      method: 'share',
      href: 'https://github.com/zyramedia/ng2-facebook-sdk'
    };

    this.fb.ui(options)
      .then((res: UIResponse) => {
        console.log('Got the users profile', res);
      })
      .catch();

  }

  twitterShare(){
    // Opens a pop-up with twitter sharing dialog
      var shareURL = "http://twitter.com/share?"; //url base
      var share ='';
      //params
      
      var params = {
        referalcode: "7878787"
        }
      for (var prop in params) share += '&' + prop + '=' + encodeURIComponent(params[prop]);
      window.open(shareURL, '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
    }

    whatsApp() {
      if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        var article = "tests";
        var weburl = "fgdgf";
        var whats_app_message = encodeURIComponent(document.URL);
        var whatsapp_url = "whatsapp://send?text=" + whats_app_message;
        window.location.href = whatsapp_url;
      } else {
        alert('You Are Not On A Mobile Device. Please Use This Button To Share On Mobile');
      }
    } 

    shareOnPinterest() {
      var str = "http://pinterest.com/pin/create/button/?url=" + encodeURIComponent(window.location.href) + "&media=''&description=''";
  
      window.open(str,
        '_blank');
  
  }


  //   whatsappShare(){
  //   var mq = window.matchMedia("(max-width: 991px)");
  //   alert(mq);
  // }
  
  // private handleError(error) {
  //   console.error('Error processing action', error);
  // }


}
