import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferalCodeComponent } from './referal-code.component';

describe('ReferalCodeComponent', () => {
  let component: ReferalCodeComponent;
  let fixture: ComponentFixture<ReferalCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferalCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferalCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
