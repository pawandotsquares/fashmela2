import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from '../data.service';
import { Md5 } from 'ts-md5/dist/md5';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import * as crypto from 'crypto-js';

@Component({
  selector: 'app-resetpassword-mobile',
  templateUrl: './resetpassword-mobile.component.html',
  styleUrls: ['./resetpassword-mobile.component.css']
})
export class ResetpasswordMobileComponent implements OnInit {

  resetPassForm: FormGroup;
  submitted = false;
  message: any;
  confirmPassNotValid: boolean;
  token: any;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private formBuilder: FormBuilder,
    private dataservice: DataService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.token = params['id'];
    });
    this.resetPassForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirm_password: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() {

    return this.resetPassForm.controls;

  }

  onSubmit() {

    this.submitted = true;

    if (this.f.password.value != this.f.confirm_password.value) {
      this.confirmPassNotValid = true;
      return;
    } else {
      this.confirmPassNotValid = false;
    }

    // stop here if form is invalid
    if (this.resetPassForm.invalid) {
      return;
    }
    this.spinner.show();
    if (this.f.password.value) {

      const md5 = new Md5();
      let password = md5.appendStr(this.f.password.value).end();

      if (this.decrypt(this.token)){
        var phobeNumber = this.decrypt(this.token)
      }

      //call login api here
      this.dataservice.changePassByMobile(phobeNumber, password).subscribe(result => {
        this.spinner.hide();
        if (result.statusCode == 200) {
          alert("Password changed successfully!")
          this.message = result;
          this.router.navigateByUrl('');
        } else {
          this.message = result;
          localStorage.setItem('statusCode', result.message);
        }

      },
        error => {

        })
    } else {
      alert('Error Occurred.');
    }
  }
  decrypt(data) {
    const secret = "@AKIAJSN26KWXJ45CJJRFUA@";

    var abc = crypto.AES.decrypt(data, secret,
      {
        keySize: 128 / 8,
        iv: secret,
        mode: crypto.mode.CBC,
        padding: crypto.pad.Pkcs7
      })
    console.log(abc);
    if (abc != ""){
        return JSON.parse(crypto.enc.Utf8.stringify(crypto.AES.decrypt(data, secret,
        {
          keySize: 128 / 8,
          iv: secret,
          mode: crypto.mode.CBC,
          padding: crypto.pad.Pkcs7
        })));
      }else{
        return;
      }
  }

}
