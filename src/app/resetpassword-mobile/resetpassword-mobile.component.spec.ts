import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetpasswordMobileComponent } from './resetpassword-mobile.component';

describe('ResetpasswordMobileComponent', () => {
  let component: ResetpasswordMobileComponent;
  let fixture: ComponentFixture<ResetpasswordMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetpasswordMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetpasswordMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
