import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  onBoarding: any = {};
  constructor(
    private DataService: DataService,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe(params => this.getProductByPurchaseType(params['id']));
   }
   getProductByPurchaseType(id: any){

	   if (id && id ==1 || id == 2){

		   localStorage.setItem('purchaseType', id);
	   }
  }

  ngOnInit() {

    if (localStorage.getItem('userOnboardingSkip') == 'false' || !localStorage.getItem('userOnboardingSkip')) {
      localStorage.setItem('userOnboardingSkip', 'true')
      document.getElementById('onBoardingClick').click();
    }
  }

}
