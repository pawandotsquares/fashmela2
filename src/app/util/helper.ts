export function myfavouriteItems(actionvalue:any, productId:any, userId:any, actiontype:any, spinner:any, dataservice:any, products:any) {
    if (actionvalue == 0) {
        actiontype = 'remove-favourite-product';
    } else {
        actiontype = 'add-favourite-product';
    }
    //alert(actiontype);
    dataservice.addfavouriteProducts(productId, userId, actiontype).subscribe(resultFeaturedProduct => {
        spinner.hide();

        products = resultFeaturedProduct.responsePacket;
        dataservice.getfavouriteProducts(localStorage.getItem('purchaseType'), userId).subscribe(resultFavouriteProducts => {
            //console.log()
            if (resultFavouriteProducts.statusCode == 200) {
                var products = resultFavouriteProducts.responsePacket;
                var favouriteCount = products.length;
                dataservice.updateNotification(favouriteCount)
            }
        },
            error => {
                //(err) => console.log(err);
            })

       // console.log(products);
    },
        error => {

        })
}

export function perCalculate(price, itemDiscount){
    var discount = (itemDiscount / price)*100
    //console.log('hello'+discount);
    return discount;
}

