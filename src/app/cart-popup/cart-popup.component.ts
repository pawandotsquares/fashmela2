import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-cart-popup',
  templateUrl: './cart-popup.component.html',
  styleUrls: ['./cart-popup.component.css']
})
export class CartPopupComponent implements OnInit {
  cartData: object;
  message: object;
  userId: any;
  cartDataCount: number;
  purchaseType: any;
  constructor(
    private DataService : DataService
  ) { }

  ngOnInit() {
    this.purchaseType = localStorage.getItem('purchaseType');
    this.userId = localStorage.getItem('userId');
    this.cartList(this.userId, this.purchaseType);
    this.cartCount(this.userId);
  }
  cartList(userId, purchaseType) {
    this.DataService.getCart(userId, purchaseType).subscribe(resultCartData => {
      if (resultCartData.statusCode == 200) {
        this.cartData = resultCartData.responsePacket;
      } else {
        this.message = resultCartData.message;
      }
    }
    )
  }
  cartCount(userId) {
    this.DataService.getCartCount(userId).subscribe(resultCartCount => {
      if (resultCartCount.statusCode == 200) {
        this.cartDataCount = resultCartCount.responsePacket.itemCount;
      } else {
        this.message = resultCartCount.message;
      }
    })
  }
}
