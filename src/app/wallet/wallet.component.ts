import { Component, OnInit } from '@angular/core';
import { DataService } from "../data.service";
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.css']
})
export class WalletComponent implements OnInit {

  walletData: object;
  message: any;
  noImg: boolean;

  public spinnerConfig: any = {
    bdColor: 'rgba(51,51,51,0.8)',
    size: 'large',
    color: '#fff',
    type: 'ball-circus',
    loadigText: 'Loading...'
  };

  constructor(
    private dataservice: DataService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {

    this.noImg = true;;
    let userId = localStorage.getItem('userId');
    this.spinner.show();
    this.walletDetails(userId)
  }
  walletDetails(userId) {

    this.dataservice.walletAmount(userId).subscribe(resultwalletData => {
      this.spinner.hide();

      if (resultwalletData.statusCode == 200) {
        if (resultwalletData.responsePacket.walletImage) {
          this.noImg = false;
        }

        this.walletData = resultwalletData.responsePacket
      } else {
        this.message = resultwalletData;
        localStorage.setItem('statusCode', resultwalletData.message);
      }
    },
      error => {

      })
  }
}
